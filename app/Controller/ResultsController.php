<?php
class ResultsController extends AppController
{
    public $helpers = array('Paginator');
    public $components = array('Paginator','HighCharts.HighCharts');
    public $currentDateTime,$studentId;
    var $paginate = array('limit'=>20,'maxLimit'=>500,'page'=>1,'order'=>array('Result.starts_time'=>'desc'));
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->authenticate();
        $this->studentId=$this->userValue['Student']['id'];
    }
    public function index()
    {
        $this->Paginator->settings = $this->paginate;
        $this->Paginator->settings['conditions'] = array('Result.student_id'=>$this->studentId,'Result.user_id >'=>0);
        $this->set('Result', $this->Paginator->paginate());  
    }
    public function view($id=null)
    {
        $studentCount=$this->Result->find('count',array('conditions'=>array('Result.id'=>$id,'Result.student_id'=>$this->studentId,'Result.user_id >'=>0)));
        if($id==null || $studentCount==0)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        $examList=$this->Result->find('all',array('conditions'=>array('Result.student_id'=>$this->studentId,'Result.user_id >'=>0),
                                                  'order'=>array('Result.start_time'=>'desc'),
                                                  'limit'=>10));
        $examDetails=$this->Result->find('first',array('fields'=>array('Exam.name','User.name','Result.percent','Exam.passing_percent','Result.result','Result.start_time','Exam.declare_result'),
                                                       'joins'=>array(array('table'=>'users','alias'=>'User','type'=>'inner',
                                                                            'conditions'=>array('Result.user_id=User.id'))),
                                                       'conditions'=>array('Result.id'=>$id,'Result.student_id'=>$this->studentId,'Result.user_id >'=>0)));
        $userSubject=$this->Result->userSubject($id);
        $userMarksheet=$this->Result->userMarksheet($id);
        $this->set('examList',$examList);
        $this->set('examDetails',$examDetails);
        $this->set('userMarksheet',$userMarksheet);
        $this->set('id',$id);
        
        foreach($userSubject as $subjectValue)
        {
            $xAxisCategories[]=$subjectValue['Subject']['subject_name'];
        }
        foreach($userMarksheet as $k=>$userMarkValue)
        {
            if(strlen($k)!=5)
            $chartData[]=(float) $userMarkValue['Subject']['percent'];
        }
        $chartName = "My Chartdl";
        $mychart = $this->HighCharts->create($chartName,'column');
        $this->HighCharts->setChartParams(
                                          $chartName,
                                          array(
                                                'renderTo'=> "mywrapperdl",  // div to display chart inside
                                                'xAxisCategories'=> $xAxisCategories,
                                                'chartHeight'=> 300,
                                                'yAxisTitleText'=> 'Percentage',
                                                'plotOptionsColumnDataLabelsEnabled'=> TRUE,                                                
                                                'legendEnabled'=> FALSE,
                                                'enableAutoStep'=> FALSE,
                                                'creditsEnabled'=> FALSE,                                                
                                                )
                                          );
        $series = $this->HighCharts->addChartSeries();
        $series->addName('Pecentage')->addData($chartData);
        $mychart->addSeries($series);
    }
    public function attemptedpapers($id=null)
    {
        $declareResult=$this->Result->find('count',array('conditions'=>array('Result.id'=>$id,'Result.student_id'=>$this->studentId,'Exam.declare_result'=>'Yes')));
        if($declareResult==0)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        $this->loadModel('ExamStat');
        $post=$this->ExamStat->find('all',array('joins'=>array(array('table'=>'questions','alias'=>'Question','type'=>'Inner',
                                                                   'conditions'=>array('Question.id=ExamStat.question_id')),
                                                             array('table'=>'qtypes','alias'=>'Qtype','type'=>'Inner',
                                                                   'conditions'=>array('Qtype.id=Question.qtype_id')),
                                                             array('table'=>'subjects','alias'=>'Subject','type'=>'Inner',
                                                                   'conditions'=>array('Subject.id=Question.subject_id')),
                                                             array('table'=>'users','alias'=>'User','type'=>'Left',
                                                                   'conditions'=>array('User.id=ExamStat.user_id'))),
                                                'fields'=>array('ExamStat.*','Question.*','Subject.*','Qtype.*'),
                                                'conditions'=>array('ExamStat.exam_result_id'=>$id,'ExamStat.student_id'=>$this->studentId),
                                                'order'=>array('ExamStat.ques_no'=>'asc')));
        $this->set('post',$post);
        $this->set('id',$id);
        $this->loadModel('User');        
        $this->set('UserArr',$this->User->find('all'));
    }
}
