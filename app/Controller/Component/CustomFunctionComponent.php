<?php
App::uses('Component','Controller');
class CustomfunctionComponent extends Component
{
    public function secondsToWords($seconds)
    {
        /*** return value ***/
        $ret = "";
    
        /*** get the hours ***/
        $hours = intval(intval($seconds) / 3600);
        if($hours > 0)
        {
            $ret .= "$hours Hours ";
        }
        /*** get the minutes ***/
        $minutes = bcmod((intval($seconds) / 60),60);
        if($hours > 0 || $minutes > 0)
        {
            $ret .= "$minutes MIns. ";
        }
        return $ret;
    }
    public function generate_rand($digit=6)
    {
      $no=substr(strtoupper(md5(uniqid(rand()))),0,$digit);
      return $no;
    }
    public function WalletInsert($student_id,$amount,$amount_type,$date,$type,$remarks,$user_id=null)
    {
        $Wallet=ClassRegistry::init('Wallet');
        $in_amount=null;
        $out_amount=null;
        if($amount_type=="Added")
        $in_amount=$amount;
        else
        $out_amount=$amount;
        if($in_amount==null && $out_amount==null)
        {
            return false;
        }
        elseif($amount<=0)
        {
            return false;
        }
        else
        {
            $Wallet->virtualFields= array('in_amount'=>'SUM(in_amount)','out_amount'=>'SUM(out_amount)');
            $AmountArr=$Wallet->find('first',array('fields'=>array('in_amount','out_amount'),'conditions'=>array('student_id'=>$student_id)));
            $total_in_amount=$AmountArr['Wallet']['in_amount'];
            $total_out_amount=$AmountArr['Wallet']['out_amount'];
            if($total_in_amount=="")
            $total_in_amount=0;
            if($total_out_amount=="")
            $total_out_amount=0;
            $balance=$total_in_amount-$total_out_amount+$in_amount-$out_amount;
            $record_arr=array('student_id'=>$student_id,'in_amount'=>$in_amount,'out_amount'=>$out_amount,'balance'=>$balance,'date'=>$date,'type'=>$type,
                              'remarks'=>$remarks,'user_id'=>$user_id);
            $Wallet->create();
            if($Wallet->save($record_arr))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public function WalletBalance($student_id)
    {
        $Wallet=ClassRegistry::init('Wallet');
        $balanceWallet=$Wallet->find('first',array('conditions'=>array('student_id'=>$student_id),
                                                   'fields'=>array('balance'),
                                                   'order'=>array('id DESC'),
                                                   'limit'=>1));
        $balance="0.00";
        if(count($balanceWallet)>0)
        {
            $balance=$balanceWallet['Wallet']['balance'];
        }
        return $balance;
    }
}