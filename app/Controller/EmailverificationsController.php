<?php
App::uses('CakeEmail', 'Network/Email');
class EmailverificationsController extends AppController
{
    var $components = array('CustomFunction');
    public function beforeFilter()
    {
        parent::beforeFilter();
        if($this->frontRegistration==0)
        return $this->redirect(array('controller'=>'','action'=>'index'));
    }
    public function index()
    {
        if ($this->request->is(array('post', 'put')))
        {
            $id=$this->request->data['Emailverification']['vc'];
            $this->EmailVerify($id);            
        }
    }
    public function emailcode($id=null)
    {
        $this->EmailVerify($id);        
    }
    public function EmailVerify($id)
    {
        $this->loadModel('Student');
        $post = $this->Student->findByRegCodeAndRegStatus($id,"Live");
        if($post)
        {
            $this->loadModel('Configuration');
            $post1=$this->Configuration->findById(1);
            $guest_login=$post1['Configuration']['guest_login'];
            if($guest_login==1)
            $this->request->data['Student']['status']="Active";
            else
            $this->request->data['Student']['reg_status']="Pending";
            $this->Student->id = $post['Student']['id'];
            $this->request->data['Student']['reg_code']="";
            $this->request->data['Student']['reg_status']="Done";
            if ($this->Student->save($this->request->data))
            {
                $this->Session->setFlash('Email verified successfully!','flash',array('alert'=>'success'));
                $this->Redirect(array('controller' => 'Users', 'action' => 'index'));
                exit();
            }
        }
        else
        {
            $this->Session->setFlash('Invalid Verification Code.','flash',array('alert'=>'danger'));
        }
        $this->redirect(array('action' => 'index'));
    }
    public function resend()
    {
        
    }
    public function resendsub()
    {
        $this->loadModel('Student');
        $id=$this->request->data['Emailverification']['email'];
        $post = $this->Student->findByEmail($id);
        if($post)
        {
            if($post['Student']['reg_status']=="Live")
            {
                $studentName=$post['Student']['name'];
                $regCode=$post['Student']['reg_code'];
                $toEmail=$post['Student']['email'];
                $rand1=$this->CustomFunction->generate_rand(35);
                $rand2=rand();
                $Email = new CakeEmail();
                $Email->from(array($this->siteEmail =>$this->siteName));
                $Email->to($toEmail);
                $Email->emailFormat('html');
                $Email->subject('Email Verification');
                $Email->send("Hi, $studentName<br>
                             Your signup email is: $toEmail<br>
                             Please click the following link to finish up registration:<br>
                             <a target=\"_blank\" href=\"$this->siteDomain/Emailverifications/emailcode/$regCode/$rand1/$rand2\">$this->siteDomain/Emailverifications/emailcode/$regCode/$rand1/$rand2</a><br>
                             <strong>Note: If the link does not open directly, please copy and paste the url into your internet browser.</strong><br>
                             Verification Code : $regCode<br>
                             Sincerely,<br>$this->siteName");
                $this->Session->setFlash('A verification Code send to your Email inbox or Spam','flash',array('alert'=>'success'));
                $this->Redirect(array('controller' => 'Emailverifications', 'action' => 'index'));
                exit();
            }
            else
            {
                $this->Session->setFlash('Email Id already verified','flash',array('alert'=>'success'));
                $this->Redirect(array('controller' => 'Users', 'action' => 'index'));
                exit();
            }
        }
        else
        {
            $this->Session->setFlash('Email Id not registered in system','flash',array('alert'=>'danger'));
        }
        $this->redirect(array('action' => 'resend'));
    }
}
