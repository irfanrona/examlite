<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    //public $components = array('Session','DebugKit.Toolbar','CustomFunction');
    public $components = array('Session','CustomFunction');
    public $siteTimezone,$siteName,$siteEmail,$siteDomain,$userValue,$adminValue,$frontRegistration,$frontSlides,$frontExamPaid,$frontLeaderBoard;
    public function authenticate()
    {
        // Check if the session variable User exists, redirect to loginform if not
        if( $this->action != 'login' )
        {
            if(!$this->Session->check('Student'))
            {
                $this->redirect(array('controller' => 'Users', 'action' => 'login'));
                exit();
            }
        }
    }
    public function beforeFilter()
    {
        $this->loadModel('Configuration');
        $sysSetting=$this->Configuration->find('first');
        $this->set('siteTitle',$sysSetting['Configuration']['meta_title']);
        $this->set('siteDescription',$sysSetting['Configuration']['meta_desc']);
        $this->set('siteName',$sysSetting['Configuration']['name']);
        $this->set('siteOrganization',$sysSetting['Configuration']['organization_name']);
        $this->set('siteAuthorName',$sysSetting['Configuration']['author']);
        $this->set('siteYear',$sysSetting['Configuration']['created']);
        $this->set('frontRegistration',$sysSetting['Configuration']['front_end']);
        $this->set('frontSlides',$sysSetting['Configuration']['slides']);
        $this->set('frontLogo',$sysSetting['Configuration']['photo']);
        $this->set('translate',$sysSetting['Configuration']['translate']);
        $this->set('frontPaidExam',$sysSetting['Configuration']['paid_exam']);
        $this->set('siteTimezone',$sysSetting['Configuration']['timezone']);
        $this->set('frontLeaderBoard',$sysSetting['Configuration']['leader_board']);
        $this->set('contact',explode("~",$sysSetting['Configuration']['contact']));
        $this->siteTimezone=$sysSetting['Configuration']['timezone'];
        $this->siteName=$sysSetting['Configuration']['name'];
        $this->siteDomain=$sysSetting['Configuration']['domain_name'];
        $this->siteEmail=$sysSetting['Configuration']['email'];
        $this->frontRegistration=$sysSetting['Configuration']['front_end'];
        $this->frontSlides=$sysSetting['Configuration']['slides'];
        $this->frontExamPaid=$sysSetting['Configuration']['paid_exam'];
        $this->frontLeaderBoard=$sysSetting['Configuration']['leader_board'];
        $this->userValue=$this->Session->read('Student');
        $this->adminValue=$this->Session->read('User');
        $walletBalance="0.00";
        if($sysSetting['Configuration']['paid_exam']>0)
        $walletBalance=$this->CustomFunction->WalletBalance($this->userValue['Student']['id']);
        $this->loadModel('Slide');
        $this->loadModel('News');
        $news=array();$slides=array();$menuArr=array();
        $slides=$this->Slide->find('all',array('conditions'=>array('status'=>'Active')));
        $news=$this->News->find('all',array('conditions'=>array('status'=>'Active'),
                                            'order'=>'id desc'));
        $menuArr=array("Dashboard"=>array("controller"=>"Dashboards","action"=>"","icon"=>"glyphicon glyphicon-home"),
                       "Leader Board"=>array("controller"=>"Leaderboards","action"=>"index","icon"=>"glyphicon glyphicon-dashboard"),
                       "My Profile"=>array("controller"=>"Profiles","action"=>"index","icon"=>"glyphicon glyphicon-th-large"),
                       "My Exams"=>array("controller"=>"Exams","action"=>"index","icon"=>"glyphicon glyphicon-list-alt"),
                       "My Result"=>array("controller"=>"Results","action"=>"index","icon"=>"glyphicon glyphicon-star-empty"),
                       "Group Performance"=>array("controller"=>"Groupperformances","action"=>"index","icon"=>"glyphicon glyphicon-cog"),
                       "Payment"=>array("controller"=>"Payments","action"=>"index","icon"=>"glyphicon glyphicon-shopping-cart"),
                       "Transaction History"=>array("controller"=>"Transactionhistorys","action"=>"index","icon"=>"glyphicon glyphicon-briefcase"),
                       "Help"=>array("controller"=>"Helps","action"=>"index","icon"=>"glyphicon glyphicon-warning-sign"));
        $frontmenuArr=array("Home"=>array("controller"=>"","action"=>"index","icon"=>"glyphicon glyphicon-home"),
                       "Sign Up"=>array("controller"=>"Registers","action"=>"index","icon"=>"glyphicon glyphicon-user"),
                       "Sign In"=>array("controller"=>"Users","action"=>"index","icon"=>"glyphicon glyphicon-lock"));
        $this->set('slides',$slides);
        $this->set('news',$news);
        $this->set('menuArr',$menuArr);
        $this->set('frontmenuArr',$frontmenuArr);
        $this->set('walletBalance',$walletBalance);        
    }
}
?>