<?php
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
class UsersController extends AppController
{
    var $name = 'Users';
    var $helpers = array('Form');
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->authenticate();
    }
    public function login()
    {
        if (empty($this->data['User']['email']) == false)
        {
            $this->loadModel('Student');
            $passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
            $password=$passwordHasher->hash($this->request->data['User']['password']);            
            $user = $this->Student->find('first', array('conditions' => array('Student.email' => $this->data['User']['email'], 'Student.password' =>$password)));            
            if($user != false)
            {
                if($user['Student']['status']=="Active")
                {
                    $this->Session->setFlash('Thank you for logging in!','flash', array('alert'=> 'success'));
                    $this->Session->write('Student', $user);                
                    $this->Redirect(array('controller' => 'Dashboards', 'action' => 'index'));
                    exit();
                }
                elseif($user['Student']['status']=="Pending" && $user['Student']['reg_status']=="Live")
                {
                    $this->Session->setFlash("Your email not verified! Please click on link sent to your email inbox or spam",'flash', array('alert'=> 'danger'));
                    $this->Redirect(array('action' => 'login'));
                    exit();
                }
                else
                {
                    $status=$user['Student']['status'];
                    $this->Session->setFlash("You are $status Member! Please contact administrator",'flash', array('alert'=> 'danger'));
                    $this->Redirect(array('action' => 'login'));
                    exit();
                }
            }
            else
            {
                $this->Session->setFlash('Incorrect username/password!','flash', array('alert'=> 'danger'));
                $this->Redirect(array('action' => 'login'));
                exit();
            }
        } 
    }
    public function logout()
    {
        $this -> Session -> destroy();
        $this -> Session -> setFlash('You have been logged out!','flash', array('alert'=> 'success'));
        $this -> Redirect(array('action' => 'login'));
        exit();
    }    
}
