<?php
App::uses('CakeEmail', 'Network/Email');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
class RegistersController extends AppController
{
    var $components = array('CustomFunction');
    var $helpers = array('Captcha');
    public function beforeFilter()
    {
        parent::beforeFilter();
        if($this->frontRegistration==0)
        return $this->redirect(array('controller'=>'','action'=>'index'));
    }
    public function captcha() {
        $this->autoRender = false;
        $this->layout='ajax';
        if(!isset($this->Captcha)) { //if Component was not loaded through $components array()
        $this->Captcha = $this->Components->load('Captcha', array(
        'width' => 150,
        'height' => 50,
        'theme' => 'random', //possible values : default, random ; No value means 'default'
        )); //load it
        }
        $this->Captcha->create();
    }
    public function index()
    {
        $this->Captcha = $this->Components->load('Captcha', array('captchaType'=>'math', 'jquerylib'=>false, 'modelName'=>'Register', 'fieldName'=>'captcha')); //load it
        if(!isset($this->Captcha))
        {
            //if Component was not loaded throug $components array()
            $this->Captcha = $this->Components->load('Captcha'); //load it
        }
        $this->loadModel('Group');       
        $this->set('group_id', $this->Group->find('list',array('fields'=>array('id','group_name'))));
        if(isset($this->request->data['Register']['captcha']))
        {
            $plainPassword=$this->request->data['Register']['password'];
            if($this->request->data['Register']['captcha']==$this->Captcha->getVerCode())
            {
                if ($this->request->is('post'))
                {
                    try
                    {
                        if(is_array($this->request->data['StudentGroup']['group_name']))
                        {
                            $passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
                            $this->request->data['Register']['password'] = $passwordHasher->hash($this->request->data['Register']['password']);
                            $this->request->data['Register']['reg_code']=$this->CustomFunction->generate_rand();
                            $this->request->data['Register']['reg_status']="Live";                    
                            unset($this->request->data['Register']['status']);
                            if ($this->Register->save($this->request->data))
                            {
                                $this->loadModel('StudentGroup');
                                $this->request->data['StudentGroup']['student_id'] = $this->Register->id;
                                if(is_array($this->request->data['StudentGroup']['group_name']))
                                {
                                    foreach($this->request->data['StudentGroup']['group_name'] as $key => $value)
                                    {
                                        $this->StudentGroup->create();
                                        $this->request->data['StudentGroup']['group_id']=$value;
                                        $this->StudentGroup->save($this->request->data);                        
                                    }
                                }
                                $studentName=$this->request->data['Register']['name'];
                                $regCode=$this->request->data['Register']['reg_code'];
                                $toEmail=$this->request->data['Register']['email'];
                                $rand1=$this->CustomFunction->generate_rand(35);
                                $rand2=rand();                       
                                $Email = new CakeEmail();
                                $Email->from(array($this->siteEmail =>$this->siteName));
                                $Email->to($toEmail);
                                $Email->emailFormat('html');
                                $Email->subject('Email Verification');
                                $Email->send("Hi, $studentName<br>
                                             Your signup email is: $toEmail<br>
                                             Please click the following link to finish up registration:<br>
                                             <a target=\"_blank\" href=\"$this->siteDomain/Emailverifications/emailcode/$regCode/$rand1/$rand2\">$this->siteDomain/Emailverifications/emailcode/$regCode/$rand1/$rand2</a><br>
                                             <strong>Note: If the link does not open directly, please copy and paste the url into your internet browser.</strong><br>
                                             Verification Code : $regCode<br>
                                             Sincerely,<br>$this->siteName");
                                $this->Session->setFlash('A verification Code send to your Email inbox or Spam','flash',array('alert'=>'success'));
                                return $this->redirect(array('controller'=>'Emailverifications','action' => 'index'));
                            }
                            $this->request->data['Register']['password']=$plainPassword;
                        }
                        else
                        {
                            $this->Session->setFlash('Please select any group.','flash',array('alert'=>'danger'));
                        }
                    }
                    catch (Exception $e)
                    {
                        $this->Session->setFlash('Invalid Post','flash',array('alert'=>'danger'));
                    }
                }            
            }
            else
            {
                $this->Session->setFlash('Invalid Security Code','flash',array('alert'=>'danger'));
            }
        }
    }
}
