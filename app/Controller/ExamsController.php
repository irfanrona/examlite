<?php
App::uses('CakeTime', 'Utility');
class ExamsController extends AppController
{
    public $helpers = array('Html');
    public $currentDateTime,$studentId;
    public $components = array('CustomFunction');
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->authenticate();
        $this->currentDateTime=CakeTime::format('Y-m-d H:i:s',CakeTime::convert(time(),$this->siteTimezone));
        $this->studentId=$this->userValue['Student']['id'];        
    }
    public function index()
    {
        $todayExam=$this->Exam->getUserExam("today",$this->studentId,$this->currentDateTime,3);
        $upcomingExam=$this->Exam->getUserExam("upcoming",$this->studentId,$this->currentDateTime,3);
        $this->set('upcomingExam',$upcomingExam);
        $this->set('todayExam',$todayExam);
    }
    public function today()
    {
        $todayExam=$this->Exam->getUserExam("today",$this->studentId,$this->currentDateTime);
        $this->set('todayExam',$todayExam);
    }
    public function upcoming()
    {
        $upcomingExam=$this->Exam->getUserExam("upcoming",$this->studentId,$this->currentDateTime);
        $this->set('upcomingExam',$upcomingExam);
    }    
    public function view($id)
    {
        $this->layout=null;
        $this->loadModel('ExamQuestion');
        $this->loadModel('ExamGroup');
        if (!$id)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        $checkPost=$this->Exam->checkPost($id,$this->studentId);
        if($checkPost==0)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'error'));
        }
        $post = $this->Exam->findByIdAndStatus($id,'Active');
        if (!$post)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'error'));
        }
        $subjectDetail=$this->Exam->getSubject($id);
        $totalQuestion=$this->ExamQuestion->find('count',array('conditions'=>array("exam_id=$id")));
        $totalMarks=$this->Exam->totalMarks($id);
        $this->set('post', $post);
        $this->set('subjectDetail', $subjectDetail);
        $this->set('totalQuestion', $totalQuestion);
        $this->set('totalMarks', $totalMarks);        
    }
    public function instruction($id)
    {
        $this->layout=null;
        $this->loadModel('ExamQuestion');
        if (!$id)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        $checkPost=$this->Exam->checkPost($id,$this->studentId);
        if($checkPost==0)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'error'));
        }
        $post = $this->Exam->findByIdAndStatus($id,'Active');
        if (!$post)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'error'));
        }
        $ispaid=false;
        $paidexam=$post['Exam']['paid_exam'];
        $this->loadModel('ExamResult');
        $this->loadModel('ExamOrder');
        $totalExam=$this->ExamResult->find('count',array('conditions'=>array('exam_id'=>$id,'student_id'=>$this->studentId)));
        $attempt_count=$post['Exam']['attempt_count'];
        if($paidexam==1)
        {
            $countExamOrder=$this->ExamOrder->find('count',array('conditions'=>array('exam_id'=>$id,'student_id'=>$this->studentId)));
            if($countExamOrder*$attempt_count>$totalExam)
            {
                $ispaid=true;
            }
        }
        $this->set('post', $post);
        $this->set('ispaid', $ispaid);
    }
    public function error()
    {
        $this->layout=null;
    }
    public function start($id=null,$quesNo=null)
    {
        if($id==null)
        $id=0;        
        $checkPost=$this->Exam->checkPost($id,$this->studentId);
        if($checkPost==0)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        $this->loadModel('ExamResult');
        $this->loadModel('ExamOrder');
        $post = $this->Exam->findById($id);
        $currentExamResult=$this->ExamResult->find('count',array('conditions'=>array('student_id'=>$this->studentId,'end_time'=>null)));
        if($currentExamResult==0)
        {
            $paidexam=$post['Exam']['paid_exam'];
            $totalExam=$this->ExamResult->find('count',array('conditions'=>array('exam_id'=>$id,'student_id'=>$this->studentId)));
            $attempt_count=$post['Exam']['attempt_count'];
            if($paidexam==1)
            {
                $countExamOrder=$this->ExamOrder->find('count',array('conditions'=>array('exam_id'=>$id,'student_id'=>$this->studentId)));
                if($countExamOrder*$attempt_count<=$totalExam)
                {
                    $this->redirect(array('action' => 'paid',$id,'P'));
                }                
            }
            else
            {
                if($attempt_count<=$totalExam)
                {
                    $this->Session->setFlash('You have attempted maximum exam.','flash',array('alert'=>'danger'));
                    $this->redirect(array('action' => 'index'));
                }
            }
            $this->Exam->userExamInsert($id,$post['Exam']['ques_random'],$this->studentId,$this->currentDateTime);
        }
        if($currentExamResult==1)
        {
            $examWise=$this->ExamResult->find('first',array('conditions'=>array('student_id'=>$this->studentId,'end_time'=>null)));
            $examWiseId=$examWise['ExamResult']['exam_id'];
            $endTime=CakeTime::format('Y-m-d H:i:s',CakeTime::fromString($examWise['ExamResult']['start_time'])+($post['Exam']['duration']*60));
            if($this->currentDateTime>=$endTime)
            $this->redirect(array('action' =>"finish/$examWiseId"));
            if($examWiseId!=$id)
            $this->redirect(array('action' =>"start/$examWiseId"));
        }
        $this->loadModel('ExamQuestion');
        $userExamQuestion=$this->Exam->userExamQuestion($id,$this->studentId,$quesNo);
        $examResult = $this->ExamResult->find('first',array('conditions'=>array('exam_id'=>$id,'student_id'=>$this->studentId,'end_time'=>null)));
        $userSectionQuestion=$this->Exam->userSectionQuestion($id,$this->studentId);
        $totalQuestion=$this->ExamQuestion->find('count',array('conditions'=>array('exam_id'=>$id)));
        $nquesNo=$quesNo;
        $pquesNo=$quesNo;
        if($quesNo==null)
        $quesNo=1;
        if($totalQuestion<$quesNo)
        $quesNo=1;
        $currSubjectName=$this->Exam->userSubject($id,$quesNo,$this->studentId);
        $this->Exam->userQuestionRead($id,$quesNo,$this->studentId);
        $oquesNo=$quesNo;
        if($totalQuestion==$quesNo)
        $quesNo=0;
        if($totalQuestion<$quesNo)
        $pquesNo=2;
        if($quesNo==1)
        $pquesNo=2;        
        $this->set('userExamQuestion',$userExamQuestion);
        $this->set('userSectionQuestion',$userSectionQuestion);
        $this->set('currSubjectName',$currSubjectName);
        $this->set('post',$post);
        $this->set('examResult',$examResult);
        $this->set('siteTimezone',$this->siteTimezone);
        $this->set('examId',$id);
        $this->set('nquesNo',$quesNo+1);
        $this->set('pquesNo',$pquesNo-1);
        $this->set('oquesNo',$oquesNo);        
    }
    function save($id=null,$quesNo=null)
    {
        $this->Exam->userSaveAnswer($id,$quesNo,$this->studentId,$this->currentDateTime,$this->request->data);
        if($this->request->data['saveNext']=="Yes")
        $quesNo++;
        $this->redirect(array('action' => "start/$id/$quesNo"));
    }
    function resetAnswer($id=null,$quesNo=null)
    {
        $this->Exam->userResetAnswer($id,$quesNo,$this->studentId);
        $this->redirect(array('action' => "start/$id/$quesNo"));
    }
    function finish($id=null)
    {
        if($id==null)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        $this->loadModel('ExamResult');
        $currentExamResult=$this->ExamResult->find('count',array('conditions'=>array('exam_id'=>$id,'student_id'=>$this->studentId,'end_time'=>null)));
        if($currentExamResult==1)
        {
            $this->Exam->userExamFinish($id,$this->studentId,$this->currentDateTime);
            $this->Session->setFlash('Exam Submitted','flash',array('alert'=>'success'));
            $this->redirect(array('action' => 'index'));
        }
        else
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
    }
    function paid($id=null,$type=null)
    {
        if($id==null)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        else
        {
            $this->loadModel('ExamOrder');
            $countExamOrder=$this->ExamOrder->find('count',array('conditions'=>array('exam_id'=>$id,'student_id'=>$this->studentId)));
            if($countExamOrder>=1 && $type==null)
            {
                $this->redirect(array('action' => 'start',$id));
            }
            else
            {
                $exampost=$this->Exam->findByIdAndPaidExam($id,'1');
                $amount=$exampost['Exam']['amount'];
                $balance=$this->CustomFunction->WalletBalance($this->studentId);
                if($balance>=$amount)
                {
                    if($this->CustomFunction->WalletInsert($this->studentId,$amount,"Deducted",$this->currentDateTime,"EM","$amount Deducted for paying exam"))
                    {
                        $this->ExamOrder->create();
                        $this->ExamOrder->save(array("student_id"=>$this->studentId,"exam_id"=>$id));
                        $this->redirect(array('action' => 'start',$id));
                    }
                }
                else
                {
                    $this->Session->setFlash('Insufficient Amount.','flash',array('alert'=>'danger'));
                    $this->redirect(array('action' => 'index'));
                }
            }
        }
        $this->redirect(array('action' => 'index'));
    }
}
