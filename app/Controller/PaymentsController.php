<?php
App::uses('CakeTime', 'Utility');
App::uses('Paypal', 'Paypal.Lib');
class PaymentsController extends AppController {   
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->authenticate();
        $this->studentId=$this->userValue['Student']['id'];
        $this->currentDateTime=CakeTime::format('Y-m-d H:i:s',CakeTime::convert(time(),$this->siteTimezone));
        $this->loadModel('PaypalConfig');
        $paySetting=$this->PaypalConfig->findById('1');
        if(strlen($paySetting['PaypalConfig']['username'])==0 || strlen($paySetting['PaypalConfig']['password'])==0 || strlen($paySetting['PaypalConfig']['signature'])==0)
        {
            $this->Session->setFlash("Paypal Payment not set",'flash',array('alert'=>'danger'));
            $this->redirect(array('controller'=>'Dashboards','action' => 'index'));
        }
        if($paySetting['PaypalConfig']['sandbox_mode']==1)
        $sandboxMode=true;
        else
        $sandboxMode=false;        
        $this->Paypal = new Paypal(array(
                                         'sandboxMode' => $sandboxMode,
                                         'nvpUsername' => $paySetting['PaypalConfig']['username'],
                                         'nvpPassword' => $paySetting['PaypalConfig']['password'],
                                         'nvpSignature' => $paySetting['PaypalConfig']['signature']
                                         ));
    }
    public function index($id=null)
    {
        if(isset($_REQUEST['token']))
        {
            $this->Session->setFlash("Payment Cancel",'flash',array('alert'=>'danger'));
        }
    }
    public function checkout()
    {
        $description=$this->request->data['Payment']['remarks'];
        $amount=$this->request->data['Payment']['amount'];
        if($amount>0)
        {
            $returnUrl=$this->siteDomain.'/Payments/postpayment/';
            $cancelUrl=$this->siteDomain.'/Payments/index/';
            $order = array(
            'description' => $description,
            'currency' => 'USD',
            'return' => $returnUrl,
            'cancel' => $cancelUrl,
            'items' => array(
                0 => array(
                    'name' => 'Wallet Payment',
                    'tax' => 0.00,
                    'shipping' => 0.00,
                    'description' => $description,
                    'subtotal' => $amount,
                ),
                )
            );
            try
            {
                $token=$this->Paypal->setExpressCheckout($order);
                $this->redirect($token);            
            }
            catch (PaypalRedirectException $e)
            {
                $this->redirect($e->getMessage());
            }
            catch (Exception $e)
            {
                $this->Session->setFlash($e->getMessage(),'flash',array('alert'=>'danger'));
            }
            $this->Session->setFlash("Try again! Can not connect to paypal",'flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        else
        {
            $this->Session->setFlash("Invalid Amount",'flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
    }
    public function postpayment($id=null)
    {
        if(isset($_REQUEST['token']) && isset($_REQUEST['PayerID']))
        {
            $token=$_REQUEST['token'];
            try
            {
                $detailsArr=$this->Paypal->getExpressCheckoutDetails($token);
                if(is_array($detailsArr))
                {
                    $amount=$detailsArr['AMT'];
                    $description=$detailsArr['DESC'];
                    $payerId=$_REQUEST['PayerID'];
                    if($detailsArr['ACK']=="Success")
                    {
                        $order = array(
                        'description' => $description,
                        'currency' => 'USD',
                        'return' => 'http://127.0.0.1/examsoft/Payments/postpayment/',
                        'cancel' => 'http://127.0.0.1/examsoft/Payments/index/',
                        'items' => array(
                            0 => array(
                                'name' => 'Wallet Payment',
                                'tax' => 0.00,
                                'shipping' => 0.00,
                                'description' => $description,
                                'subtotal' => $amount,
                            ),
                            )
                        );
                        try
                        {
                            $paymentDetails=$this->Paypal->doExpressCheckoutPayment($order,$token,$payerId);
                            if(is_array($paymentDetails))
                            {
                                if($paymentDetails['PAYMENTINFO_0_PAYMENTSTATUS']=="Completed" && $paymentDetails['PAYMENTINFO_0_ACK']=="Success")
                                {
                                    $transactionId=$paymentDetails['PAYMENTINFO_0_TRANSACTIONID'];
                                    $total=$this->Payment->find('count',array('conditions'=>array('Payment.transaction_id'=>$transactionId)));
                                    if($total==0)
                                    {
                                        $record_arr=array('student_id'=>$this->studentId,'transaction_id'=>$transactionId,'amount'=>$amount,'remarks'=>$description);
                                        $this->Payment->save($record_arr);
                                        $this->CustomFunction->WalletInsert($this->studentId,$amount,"Added",$this->currentDateTime,"PG",$description);
                                        $this->Session->setFlash("Payment successfully! Amount $amount added in your wallet",'flash',array('alert'=>'success'));
                                    }
                                    else
                                    {
                                        $this->Session->setFlash("Payment already done!",'flash',array('alert'=>'danger'));
                                    }
                                }
                            }
                        }
                        catch (PaypalRedirectException $e)
                        {
                            $this->redirect($e->getMessage());
                        }
                        catch (Exception $e)
                        {
                            $this->Session->setFlash($e->getMessage(),'flash',array('alert'=>'danger'));
                        }                        
                    }
                    else
                    {
                        $this->Session->setFlash("Payment not done",'flash',array('alert'=>'danger'));
                    }
                }                
            }
            catch (Exception $e)
            {
                $this->Session->setFlash($e->getMessage(),'flash',array('alert'=>'danger'));
            }
        }
        $this->redirect(array('action' => 'index'));
    }
}