<?php
class Question extends AppModel
{
  public $actsAs = array('search-master.Searchable');
  public $belongsTo="Subject";
  public $filterArgs = array('keyword' => array('type' => 'like','field'=>'Question.question'));
}
?>