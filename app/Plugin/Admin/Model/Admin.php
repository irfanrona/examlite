<?php
class Admin extends AppModel
{
    public function viewdifftype($subject_id,$type)
    {
        $Question=ClassRegistry::init('Question');        
        return $Question->find('count',array('joins'=>array(array('table'=>'diffs','type'=>'Inner','alias'=>'Diff','conditions'=>array('Diff.id=Question.diff_id'))),
                                            'conditions'=>array('Question.subject_id'=>$subject_id,'Diff.type'=>$type)));
    }
    public function studentGroups()
    {
        $StudentGroup=ClassRegistry::init('Group');
        return$StudentGroup->find('all');
    }
    public function studentGroupCount($groupId,$status=null)
    {
        $Student=ClassRegistry::init('Student');
        if($status==null)
        $statusCond=null;
        else
        $statusCond=array('Student.status'=>$status);
        return$Student->find('count',array('joins'=>array(array('table'=>'student_groups','alias'=>'StudentGroup','type'=>'Inner',
                                                          'conditions'=>array('Student.id=StudentGroup.student_id'))),
                                     'conditions'=>array('StudentGroup.group_id'=>$groupId,$statusCond)));
    }
    public function studentStatitics()
    {
        $studentGroup=$this->studentGroups();
        $studentStatitics=array();
        foreach($studentGroup as $k=>$groupValue)
        {
            $studentStatitics[$k]['GroupName']['name']=$groupValue['Group']['group_name'];
            $studentStatitics[$k]['GroupName']['total_student']=$this->studentGroupCount($groupValue['Group']['id']);
            $studentStatitics[$k]['GroupName']['active']=$this->studentGroupCount($groupValue['Group']['id'],'Active');
            $studentStatitics[$k]['GroupName']['pending']=$this->studentGroupCount($groupValue['Group']['id'],'Pending');
            $studentStatitics[$k]['GroupName']['suspend']=$this->studentGroupCount($groupValue['Group']['id'],'Suspend');
        }
        return$studentStatitics;
    }
    public function studentStat($examId,$type)
    {
        $ExamResult=ClassRegistry::init('ExamResult');
        return$ExamResult->find('count',array('conditions'=>array('exam_id'=>$examId,'result'=>$type)));
    }
    public function studentAverageResult($examId)
    {
        $ExamResult=ClassRegistry::init('ExamResult');
        $totalAttempt=$ExamResult->find('count',array('conditions'=>array('exam_id'=>$examId)));
        $ExamResult->virtualFields= array('total'=>'SUM(ExamResult.percent)');
        $studentPercent=$ExamResult->find('first',array('conditions'=>array('exam_id'=>$examId)));
        $totalPercent=$studentPercent['ExamResult']['total'];
        if($totalAttempt>0)
        $averagePercent=CakeNumber::precision($totalPercent/$totalAttempt,2);
        else
        $averagePercent=0;
        return$averagePercent;
        
    }
    public function examTotalAbsent($examId)
    {
      $Exam=ClassRegistry::init('Exam');
      $ExamResult=ClassRegistry::init('ExamResult');
      $userTotalExam=$Exam->find('count',array(
                              'joins'=>array(array('table'=>'exam_groups','alias'=>'ExamGroup','type'=>'Inner',
                                                   'conditions'=>array('Exam.id=ExamGroup.exam_id')),                                             
                                             array('table'=>'student_groups','alias'=>'StudentGroup','type'=>'Inner',
                                                   'conditions'=>array('StudentGroup.group_id=ExamGroup.group_id'))),
                              'conditions'=>array('Exam.status'=>'Closed',"ExamGroup.exam_id=$examId"),                              
                              'group'=>array('StudentGroup.student_id')));
        $userAttemptExamArr=$ExamResult->find('first',array('fields'=>array('COUNT(DISTINCT(`student_id`)) AS `total`'),
                                                      'conditions'=>array('ExamResult.exam_id'=>$examId)));
        $userAttemptExam=$userAttemptExamArr[0]['total'];
        $userTotalAbsent=$userTotalExam-$userAttemptExam;
        return$userTotalAbsent;
    }
    public function recentExamResult()
    {
        $Exam=ClassRegistry::init('Exam');
        $examList=$Exam->find('all',array('fields'=>array('id','name','start_date','end_date','passing_percent'),
                             'conditions'=>array('Exam.status'=>'Closed'),
                             'order'=>array('Exam.end_date'=>'desc'),
                             'limit'=>3));
        $recentExamResult=array();
        foreach($examList as $k=>$examvalue)
        {
            $recentExamResult[$k]['RecentExam']['Exam']['id']=$examvalue['Exam']['id'];
            $recentExamResult[$k]['RecentExam']['Exam']['name']=$examvalue['Exam']['name'];
            $recentExamResult[$k]['RecentExam']['Exam']['start_date']=$examvalue['Exam']['start_date'];
            $recentExamResult[$k]['RecentExam']['Exam']['end_date']=$examvalue['Exam']['end_date'];
            $recentExamResult[$k]['RecentExam']['OverallResult']['passing']=(float) $examvalue['Exam']['passing_percent'];
            $recentExamResult[$k]['RecentExam']['OverallResult']['average']=(float) $this->studentAverageResult($examvalue['Exam']['id']);
            $recentExamResult[$k]['RecentExam']['StudentStat']['pass']=$this->studentStat($examvalue['Exam']['id'],'Pass');
            $recentExamResult[$k]['RecentExam']['StudentStat']['fail']=$this->studentStat($examvalue['Exam']['id'],'Fail');
            $recentExamResult[$k]['RecentExam']['StudentStat']['absent']=(float) $this->examTotalAbsent($examvalue['Exam']['id']);
        }
        return$recentExamResult;
    }
}
