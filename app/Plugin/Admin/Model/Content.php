<?php
class Content extends AppModel
{
  public $useTable="configurations";
  public $actsAs = array('search-master.Searchable','Upload.Upload' => array(
            'photo' => array(
                'pathMethod'=>'flat',
                'path' => '{ROOT}webroot{DS}img{DS}',
                'deleteOnUpdate' => true,
            ),
        )
    );
  public $validate = array('photo' => array('allowEmpty' => true,
                                           'rule' => array('isValidExtension', array('jpg', 'jpeg', 'png'),false),
                                           'message' => 'File does not have a valid extension',
                                           'rule' => array('isValidMimeType', array('image/jpeg','image/png','image/bmp','image/gif'),false),
                                           'message' => 'You must supply a JPG, GIF  or PNG File.',
                                           'rule'=>array('isBelowMaxHeight',220),
                                           'message'=>'You must supply image height is less than 220px')
                          );
}
?>