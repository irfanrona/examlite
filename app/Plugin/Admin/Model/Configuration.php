<?php
class Configuration extends AppModel
{
    public $validate = array('name' => array('alphaNumeric' => array('rule' => '/^[a-z0-9 .-]*$/i','required' => true,'allowEmpty'=>false,'message' => 'Only letters and numbers allowed')),
                           'organization_name' => array('alphaNumeric' => array('rule' => '/^[a-z0-9 ]*$/i','required' => true,'message' => 'Only letters and numbers allowed')),
                           'email' => array('rule' => 'email','message' => 'Enter a valid email','allowEmpty' => true),
                           'domain_name' => array('rule' => 'url','required' => true,'message' => 'Only URL allowed'),
                           'meta_title' => array('alphaNumeric' => array('rule' => '/^[a-z0-9 .-]*$/i','required' => true,'allowEmpty'=>true,'message' => 'Only letters and numbers allowed')),
                           'meta_desc' => array('alphaNumeric' => array('rule' => '/^[a-z0-9 .-]*$/i','required' => true,'allowEmpty'=>true,'message' => 'Only letters and numbers allowed')),
                           'contact' => array('alphaNumeric' => array('rule' => '/^[a-z0-9 \/ \s\\\\,-.:@~]*$/i','required' => true,'allowEmpty'=>true,'message' => 'Invalid Header Contact')),
                           'timezone' => array('alphaNumeric' => array('rule' => '/^[a-z0-9 ()+-:,]*$/i','required' => true,'message' => 'Only letters and numbers allowed')),
                           );
}
?>