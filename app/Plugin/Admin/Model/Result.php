<?php
class Result extends AppModel
{
  public $name="Result";
  public $useTable="exam_results";
  public $belongsTo=array('Student'=>
                          array('className'=>'students'),
                          'Exam'=>
                          array('className'=>'exams'),
                          );
  public $hasAndBelongsToMany = array('Group'=>array('className'=>'Group',
                                                     'joinTable' => 'student_groups',
                                                     'foreignKey' => 'student_id',
                                                     'associationForeignKey' => 'group_id'));
  public function examOptions()
  {
    $Exam=ClassRegistry::init('Exam');
    $examOptions=$Exam->find('list',array('fields'=>array('id','name'),
                             'conditions'=>array('Exam.status'=>'Closed'),
                             'order'=>array('Exam.name asc')));
    return$examOptions;
  }
  public function difficultyWiseQuestion($id,$type)
  {
    $ExamStat=ClassRegistry::init('ExamStat');
    $quesCount=$ExamStat->find('count',array(
                                  'joins'=>array(array('table'=>'questions','alias'=>'Question','type'=>'Inner',
                                                       'conditions'=>array('ExamStat.question_id=Question.id')),
                                                 array('table'=>'diffs','alias'=>'Diff','type'=>'Inner',
                                                       'conditions'=>array('Question.diff_id=Diff.id'))),
                                  'conditions'=>array('ExamStat.exam_result_id'=>$id,'Diff.type'=>$type,'ExamStat.answered'=>1)));
    return$quesCount;
  }
  public function studentDetail($id)
  {
    $ExamResult=ClassRegistry::init('ExamResult');
    $studentDetail=$ExamResult->find('first',array('fields'=>array('Student.id','Student.name'),
                                    'joins'=>array(array('table'=>'students','alias'=>'Student','type'=>'Inner',
                                                         'conditions'=>array('ExamResult.student_id=Student.id'))),
                                    'conditions'=>array('ExamResult.id'=>$id)));
    return$studentDetail;
  }
  public function performanceCount($studentId,$month)
  {
    $ExamResult=ClassRegistry::init('ExamResult');
    $conditions=array('ExamResult.student_id'=>$studentId,'MONTH(ExamResult.start_time)'=>$month,'ExamResult.user_id >'=>0);
    $examCount=$ExamResult->find('count',array('conditions'=>array($conditions)));
    $ExamResult->virtualFields= array('total'=>'SUM(ExamResult.percent)');
    $exampercent=$ExamResult->find('first',array('field'=>array('total'),
                                                 'conditions'=>array($conditions)));
    $percent=$exampercent['ExamResult']['total'];
    if($examCount>0)
    $averagePercent=CakeNumber::precision($percent/$examCount,2);
    else
    $averagePercent=0;
    return$averagePercent;
  }
  public function studentWise($name=null,$studentGroup=null)
  {
    $Student=ClassRegistry::init('Student');
    $conditions=array();
    $Student->bindModel(array('hasAndBelongsToMany' => array('Group'=>array('className'=>'Group',
                                                                                          'joinTable' => 'student_groups',
                                                                                          'foreignKey' => 'student_id',
                                                                                          'associationForeignKey' => 'group_id')
                                                                )
                                 )
                           );
    if($name!=null)
    $conditions[]=("Student.name LIKE '%$name%'");
    if($studentGroup!=null)
    $conditions[]=array('StudentGroup.group_id'=>$studentGroup);
    $studentDetail=$Student->find('all',array('fields'=>array('DISTINCT(Student.id)','Student.name','Student.enroll'),
                                              'joins'=>array(array('table'=>'student_groups','alias'=>'StudentGroup','type'=>'inner',
                                                                   'conditions'=>array('Student.id=StudentGroup.student_id'))),
                                              'conditions'=>array($conditions)));
    return$studentDetail;
  }
}
?>