<?php
class Student extends AppModel
{
   public $actsAs = array('search-master.Searchable','Upload.Upload' => array(
            'photo' => array(
                'pathMethod'=>'flat',
                'thumbnailSizes' => array(
                    '' => '150x150',
                ),                            
                'thumbnailMethod' => 'php',
                'thumbnailPrefixStyle' => false,
                'thumbnailType'=>true
            ),
        )
    );   
 public $validate = array('photo' => array('allowEmpty' => true,
                                           'rule' => array('isValidExtension', array('jpg', 'jpeg', 'png'),false),
                                           'message' => 'File does not have a valid extension',
                                           'rule' => array('isValidMimeType', array('image/jpeg','image/png','image/bmp','image/gif'),false),
                                           'message' => 'You must supply a JPG, GIF  or PNG File.'),
                          'group_name'=>array('rule' => 'notEmpty'),
                          );
  public $filterArgs = array('keyword' => array('type' => 'like','field'=>'Student.email'));
  public $hasAndBelongsToMany = array('Group'=>array('className'=>'Group',
                                                     'joinTable' => 'student_groups',
                                                     'foreignKey' => 'student_id',
                                                     'associationForeignKey' => 'group_id'));
}
?>