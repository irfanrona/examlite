<?php
App::uses('CakeTime', 'Utility');
App::uses('CakeNumber', 'Utility');
class ResultsController extends AdminAppController
{
    public $components = array('HighCharts.HighCharts');
    public function index($studentId=null)
    {
        $this->loadModel('Group');
        $this->set('group_id', $this->Group->find('list',array('fields'=>array('id','group_name'))));
        $this->set('examOptions',$this->Result->examOptions());
        $this->set('examId',null);
        if(isset($this->request->data['Result']['id']) || isset($this->request->data['id']))
        {
            $this->loadModel('Exam');
            if(isset($this->request->data['Result']['id']))
            $examId=$this->request->data['Result']['id'];
            if(isset($this->request->data['id']))
            $examId=$this->request->data['id'];
            $post=$this->Result->findByexamId($examId);
            $examResult=$this->Result->find('all',array('conditions'=>array('Result.exam_id'=>$examId,'Result.user_id >'=>0),
                                                        'order'=>array('Result.percent desc')));
            $this->set('post',$post);
            $this->set('examResult',$examResult);
            $this->set('examId',$examId);
        }
        $name=null;$studentGroup=null;
        if(isset($this->request->data['Result']['name']) && strlen($this->request->data['Result']['name'])>0)
        {
            $name=$this->request->data['Result']['name'];
            $isSearch=true;
        }
        if(isset($this->request->data['StudentGroup']['group_name']) && is_array($this->request->data['StudentGroup']['group_name']))
        {
            $isSearch=true;
            foreach($this->request->data['StudentGroup']['group_name'] as $value)
            {
                $studentGroup[]=$value;
            }           
        }
        if(isset($isSearch) && $isSearch==true)
        {
            $studentDetails=$this->Result->studentWise($name,$studentGroup);
            $this->set('studentDetails',$studentDetails);
        }
        if($studentId!=null)
        {
            $examResult=$this->Result->find('all',array('conditions'=>array('Result.student_id'=>$studentId,'Result.user_id >'=>0),
                                                        'order'=>array('Result.start_time desc')));
            $this->set('examResult',$examResult);
            $this->set('studentId',$studentId);
        }
        
    }
    public function view($id=null)
    {
      $this->layout=null;
      $this->loadModel('ExamResult');
      $totalExam=$this->ExamResult->find('count',array('conditions'=>array('id'=>$id)));
      if($totalExam==0)
      {
        $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
        return $this->redirect(array('action' => 'index'));
      }
      $easy=$this->Result->difficultyWiseQuestion($id,'E');
      $normal=$this->Result->difficultyWiseQuestion($id,'M');
      $difficult=$this->Result->difficultyWiseQuestion($id,'D');
      
      $chartData=array(array('Easy',$easy),array('Normal',$normal),array('Difficult',$difficult));
      $chartName = "Pie Chartqc";
      $pieChart = $this->HighCharts->create($chartName,'pie');
      $this->HighCharts->setChartParams(
                                        $chartName,
                                        array(
                                              'renderTo'=> "piewrapperqc",  // div to display chart inside
                                              'title'=> 'Question Difficulty Level',
                                              'titleAlign'=> 'center',
                                              'creditsEnabled'=> FALSE,
                                              'legendEnabled'=>TRUE,
                                              'legendLayout'=> 'vertical',
                                              'legendVerticalAlign'=> 'middle',
                                              'legendAlign'=> 'right',
                                              'plotOptionsPieShowInLegend'=> TRUE,
                                              'plotOptionsPieDataLabelsEnabled'=> TRUE,
                                              'plotOptionsPieDataLabelsFormat'=>'<b>{point.name}</b>: {point.y}',
                                              )
                                        );
      $series = $this->HighCharts->addChartSeries();
      $series->addName('Total Question')->addData($chartData);
      $pieChart->addSeries($series);
      
      $studentDetail=$this->Result->studentDetail($id);
      $studentName=$studentDetail['Student']['name']."'s Performance";
      $studentId=$studentDetail['Student']['id'];
      $performanceChartData=array();
      $currentMonth=CakeTime::format('m',CakeTime::convert(time(),$this->siteTimezone));
      for($i=1;$i<=12;$i++)
      {
        if($i>$currentMonth)
        break;
        $examData=$this->Result->performanceCount($studentId,$i);
        $performanceChartData[]=(float) $examData;
      }
      $tooltipFormatFunction ="function() { return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y +'% Marks';}";
      $chartName = "My Chartdl";
      $mychart = $this->HighCharts->create($chartName,'line');
      $this->HighCharts->setChartParams(
                                        $chartName,
                                        array(
                                              'renderTo'=> "mywrapperdl",  // div to display chart inside
                                              'title'=> $studentName,
                                              'titleAlign'=> 'center',
                                              'creditsEnabled'=> FALSE,
                                              'xAxisLabelsEnabled'=> TRUE,
                                              'xAxisCategories'=> array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'),
                                              'yAxisTitleText'=> 'Percentage',
                                              'tooltipEnabled'=> TRUE,
                                                'tooltipFormatter'=> $tooltipFormatFunction,
                                              'enableAutoStep'=> FALSE,
                                              'plotOptionsShowInLegend'=> TRUE,                                              
                                              )
                                        );
      $series = $this->HighCharts->addChartSeries();
      $series->addName('Exam')->addData($performanceChartData);      
      $mychart->addSeries($series);
    }
}