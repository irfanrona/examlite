<?php
class HelpcontentsController extends AdminAppController {
    public $helpers = array('Html', 'Form','Session','Paginator');
    public $components = array('Session','Paginator','search-master.Prg');
    public $presetVars = true;
    var $paginate = array('limit'=>20,'maxLimit'=>500,'page'=>1,'order'=>array('Helpcontent.id'=>'desc'));
    public function index()
    {
        $this->Prg->commonProcess();
        $this->Paginator->settings = $this->paginate;
        $this->Paginator->settings['conditions'] = $this->Helpcontent->parseCriteria($this->Prg->parsedParams());
        $this->set('Helpcontent', $this->Paginator->paginate());        
    }    
    public function add()
    {
        if ($this->request->is('post'))
        {
            $this->Helpcontent->create();
            try
            {
                if ($this->Helpcontent->save($this->request->data))
                {
                    $this->Session->setFlash('Your help content has been saved.','flash',array('alert'=>'success'));
                    return $this->redirect(array('action' => 'index'));
                }
            }
            catch (Exception $e)
            {
                $this->Session->setFlash('help content already exist.','flash',array('alert'=>'danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }
    }
    public function edit($id = null)
    {
        if (!$id)
        {
            throw new NotFoundException(__('Invalid post'));
        }
        $ids=explode(",",$id);
        $post=array();
        foreach($ids as $id)
        {
            $post[]=$this->Helpcontent->findByid($id);
        }
        $this->set('Helpcontent',$post);
        if (!$post)
        {
            throw new NotFoundException(__('Invalid post'));
        }
        if ($this->request->is(array('post', 'put')))
        {
            $this->Helpcontent->id = $id;
            try
            {
                foreach($this->data['Helpcontent'] as $key => $value)
                {
                    if ($this->Helpcontent->save($this->request->data['Helpcontent'][$key]))
                    {
                        $this->Session->setFlash('Your help content has been updated.','flash',array('alert'=>'success'));                        
                    }
                }
                return $this->redirect(array('action' => 'index'));
            }
            catch (Exception $e)
            {
                $this->Session->setFlash('help content already exist.','flash',array('alert'=>'danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }
        if (!$this->request->data)
        {
            $this->request->data = $post;
        }
    }    
    public function deleteall()
    {
        if ($this->request->is('post'))
        {
            foreach($this->data['Helpcontent']['id'] as $key => $value)
            {
                $this->Helpcontent->delete($value);
            }
            $this->Session->setFlash('Your help content has been deleted.','flash',array('alert'=>'success'));
        }        
        $this->redirect(array('action' => 'index'));
    }
}
