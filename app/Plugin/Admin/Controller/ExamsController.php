<?php
class ExamsController extends AdminAppController {
    public $helpers = array('Html', 'Form','Session','Paginator','Time','Tinymce');
    public $components = array('Session','Paginator','search-master.Prg','HighCharts.HighCharts');
    public $presetVars = true;
    var $paginate = array('limit'=>20,'maxLimit'=>500,'page'=>1,'order'=>array('Exam.id'=>'desc'));
    public function index()
    {
        $this->Prg->commonProcess();
        $this->Paginator->settings = $this->paginate;
        $this->Paginator->settings['conditions'] = $this->Exam->parseCriteria($this->Prg->parsedParams());
        $this->set('Exam', $this->Paginator->paginate());        
    }
    public function add()
    {
        $this->loadModel('Group');
        $this->set('group_id', $this->Group->find('list',array('fields'=>array('id','group_name'))));
        if ($this->request->is('post'))
        {
            $this->Exam->create();
            try
            {
                if(strtotime($this->request->data['Exam']['end_date'])>strtotime($this->request->data['Exam']['start_date']))
                {
                    if ($this->Exam->save($this->request->data))
                    {
                        $this->loadModel('ExamGroup');
                        $this->request->data['ExamGroup']['exam_id'] = $this->Exam->id;
                        if(is_array($this->request->data['ExamGroup']['group_name']))
                        {
                            foreach($this->request->data['ExamGroup']['group_name'] as $key => $value)
                            {
                                $this->ExamGroup->create();
                                $this->request->data['ExamGroup']['group_id']=$value;
                                $this->ExamGroup->save($this->request->data);                        
                            }
                        }
                        $this->Session->setFlash('Your Exam has been saved.','flash',array('alert'=>'success'));
                        return $this->redirect(array('action' => 'index'));
                    }
                }
                else
                {
                    $this->Session->setFlash('End Date is not less than Start date.','flash',array('alert'=>'danger'));
                    return $this->redirect(array('action' => 'index'));
                }
            }
            catch (Exception $e)
            {
                $this->Session->setFlash('Exam Name already exist.','flash',array('alert'=>'danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }
        $this->set('frontExamPaid',$this->frontExamPaid);
    }
    public function edit($id = null)
    {
        $this->loadModel('Group');
        $this->set('group_id', $this->Group->find('list',array('fields'=>array('id','group_name'))));
        if (!$id)
        {
            throw new NotFoundException(__('Invalid post'));
        }
        $ids=explode(",",$id);
        $post=array();
        foreach($ids as $id)
        {
            $post[]=$this->Exam->findByid($id);            
        }        
        $this->set('Exam',$post);
        if (!$post)
        {
            throw new NotFoundException(__('Invalid post'));
        }
        if ($this->request->is(array('post', 'put')))
        {
            $this->Exam->id = $id;
            try
            {
                foreach($this->data['Exam'] as $key => $value)
                {
                    if(strtotime($this->request->data['Exam'][$key]['end_date'])>strtotime($this->request->data['Exam'][$key]['start_date']))
                    {
                        if ($this->Exam->save($this->request->data['Exam'][$key]))
                        {
                            $this->loadModel('ExamGroup');
                            $this->ExamGroup->deleteAll(array('ExamGroup.exam_id'=>$key));
                            if(is_array($this->request->data['ExamGroup'][$key]['group_name']))
                            {
                                foreach($this->request->data['ExamGroup'][$key]['group_name'] as $key1 => $value1)
                                {
                                    $this->ExamGroup->create();
                                    $this->request->data['ExamGroup'][$key1]['exam_id']=$key;
                                    $this->request->data['ExamGroup'][$key1]['group_id']=$value1;                            
                                    $this->ExamGroup->save($this->request->data['ExamGroup'][$key1]);
                                }
                            }
                            $this->Session->setFlash('Your Exam has been updated.','flash',array('alert'=>'success'));                        
                        }
                    }
                    else
                    {
                        $this->Session->setFlash('End Date is not less than Start date.','flash',array('alert'=>'danger'));
                        return $this->redirect(array('action' => 'index'));
                    }
                }                
                return $this->redirect(array('action' => 'index'));
            }
            catch (Exception $e)
            {
                $this->Session->setFlash('Exam can not updated.','flash',array('alert'=>'danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }
        if (!$this->request->data)
        {
            $this->request->data = $post;
        }
        $this->set('frontExamPaid',$this->frontExamPaid);
    }
    public function deleteall()
    {
        if ($this->request->is('post'))
        {
            foreach($this->data['Exam']['id'] as $key => $value)
            {
                $this->Exam->delete($value);
            }
            $this->Session->setFlash('Your Exam has been deleted.','flash',array('alert'=>'success'));
        }        
        $this->redirect(array('action' => 'index'));
    }
    public function view($id = null)
    {
        $this->layout = null;
        if (!$id)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        $post = $this->Exam->findById($id);
        $this->loadModel('ExamQuestion');
        $this->loadModel('Diff');
        $TotalQuestion=$this->ExamQuestion->find('count',array('conditions'=>array("exam_id=$id")));
        $SubjectDetail=$this->ExamQuestion->find('all',array(
                                                             'fields'=>array('DISTINCT(Question.subject_id)','Subject.subject_name'),
                                                             'joins'=>array(array('table'=>'questions','type'=>'Inner','alias'=>'Question','conditions'=>array('Question.id=ExamQuestion.question_id')),
                                                                 array('table'=>'subjects','type'=>'Inner','alias'=>'Subject','conditions'=>array('Subject.id=Question.subject_id'))),
                                                  'conditions'=>array('ExamQuestion.exam_id'=>$id)));        
        $DiffLevel=$this->Diff->find('all');
        $i=0;
        foreach($SubjectDetail as $value)
        {
            $subject_id=$value['Question']['subject_id'];
            $subject_name= h($value['Subject']['subject_name']);
            $QuestionDetail[$subject_name][]=$this->viewquestiontype($id,$subject_id,'S');
            $QuestionDetail[$subject_name][]=$this->viewquestiontype($id,$subject_id,'M');
            $QuestionDetail[$subject_name][]=$this->viewquestiontype($id,$subject_id,'T');
            $QuestionDetail[$subject_name][]=$this->viewquestiontype($id,$subject_id,'F');
            $DifficultyDetail[$subject_name][]=$this->viewdifftype($id,$subject_id,'E');
            $DifficultyDetail[$subject_name][]=$this->viewdifftype($id,$subject_id,'M');
            $DifficultyDetail[$subject_name][]=$this->viewdifftype($id,$subject_id,'D');
            $j=0;
            foreach($DiffLevel as $diff)
            {
                $tot_ques=(float) $DifficultyDetail[$subject_name][$j];
                $chartData[]=array($diff['Diff']['diff_level'],$tot_ques);
                $j++;
                
            }
            $chartName = "Pie Chart$i";
            $pieChart = $this->HighCharts->create( $chartName, 'pie' );
            $this->HighCharts->setChartParams(
            $chartName,
                array(
                'renderTo'				=> "piewrapper$i",  // div to display chart inside
                'chartWidth'			=> 250,
                'chartHeight'			=> 300,
                'creditsEnabled'=> FALSE,
                'title'				=> $subject_name,
                'titleAlign'			=> 'left',
                'plotOptionsPieShowInLegend'=> TRUE,
                'plotOptionsPieDataLabelsEnabled'=> TRUE,
                'plotOptionsPieDataLabelsFormat'=>'<b>{point.y}</b>',                                                
                )
            );
            $series = $this->HighCharts->addChartSeries();        
            $series->addName('Difficulty Level')->addData($chartData);
            $pieChart->addSeries($series);
            unset($chartData);
            $i++;
        }
        if (!$post)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('post', $post);
        $this->set('id', $id);
        $this->set('TotalQuestion',$TotalQuestion);
        $this->set('SubjectDetail',$SubjectDetail);
        if(isset($QuestionDetail))
        $this->set('QuestionDetail',$QuestionDetail);
        $this->set('DiffLevel',$DiffLevel);
        if(isset($DifficultyDetail))
        $this->set('DifficultyDetail',$DifficultyDetail);
    }
    public function activateexam($id=null)
    {
        $examCount=$this->Exam->find('count',array('conditions'=>array('id'=>$id)));
        if($id==null || $examCount==0)
        {
            $this->Session->setFlash('Invalid Post','flash',array('alert'=>'danger'));
            $this->redirect(array('controller'=>'Exams','action' => 'index'));
        }
        $this->Exam->save(array('id'=>$id,'status'=>'Active'));
        $this->Session->setFlash('Exam sucessfully activated','flash',array('alert'=>'success'));
        $this->redirect(array('controller'=>'Exams','action' => 'index'));
    }
    private function viewquestiontype($id,$subject_id,$type)
    {
        $this->loadModel('Question');
        return $this->Question->find('count',array(
                                                  'joins'=>array(array('table'=>'qtypes','type'=>'Inner','alias'=>'Qtype','conditions'=>array('Question.qtype_id=Qtype.id')),
                                                                 array('table'=>'exam_questions','type'=>'Inner','alias'=>'ExamQuestion','conditions'=>array('Question.id=ExamQuestion.question_id'))),
                                                  'conditions'=>array('ExamQuestion.exam_id'=>$id,'Question.subject_id'=>$subject_id,'Qtype.type'=>$type)));        
    }
    private function viewdifftype($id,$subject_id,$type)
    {
        $this->loadModel('Question');
        return $this->Question->find('count',array(
                                                  'joins'=>array(array('table'=>'diffs','type'=>'Inner','alias'=>'Diff','conditions'=>array('Question.diff_id=Diff.id')),
                                                                 array('table'=>'exam_questions','type'=>'Inner','alias'=>'ExamQuestion','conditions'=>array('Question.id=ExamQuestion.question_id'))),
                                                  'conditions'=>array('ExamQuestion.exam_id'=>$id,'Question.subject_id'=>$subject_id,'Diff.type'=>$type)));        
    }
}
