<?php
class SubjectsController extends AdminAppController {
    public $helpers = array('Html', 'Form','Session','Paginator');
    public $components = array('Session','Paginator','search-master.Prg');
    public $presetVars = true;
    var $paginate = array('fields'=>array('Subject.id','Subject.subject_name','qbank_count'),'recursive'=>-1,
                          'joins'=>array(array('table'=>'questions','type'=>'LEFT','alias'=>'Question','conditions'=>array('Subject.id=Question.subject_id'))),
                          'limit'=>20,'maxLimit'=>500,'page'=>1,'order'=>array('Subject.subject_name'=>'asc'),'group'=>array('Subject.id'));
    public function index()
    {
        $this->Prg->commonProcess();
        $this->Subject->virtualFields= array('qbank_count' => 'Count(Question.id)');
        $this->Paginator->settings = $this->paginate;        
        $this->Paginator->settings['conditions'] = $this->Subject->parseCriteria($this->Prg->parsedParams());
        $this->set('Subject', $this->Paginator->paginate());        
    }    
    public function add()
    {
        $this->layout = null;
        if ($this->request->is('post'))
        {
            $this->Subject->create();
            try
            {
                if ($this->Subject->save($this->request->data))
                {
                    $this->Session->setFlash('Your Subject has been saved.','flash',array('alert'=>'success'));
                    return $this->redirect(array('action' => 'index'));
                }
            }
            catch (Exception $e)
            {
                $this->Session->setFlash('Subject Name already exist.','flash',array('alert'=>'danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }
    }
    public function edit($id = null)
    {
        $this->layout = null;
        if (!$id)
        {
            throw new NotFoundException(__('Invalid post'));
        }
        $ids=explode(",",$id);
        $post=array();
        foreach($ids as $id)
        {
            $post[]=$this->Subject->findByid($id);
        }
        $this->set('Subject',$post);
        if (!$post)
        {
            throw new NotFoundException(__('Invalid post'));
        }
        if ($this->request->is(array('post', 'put')))
        {
            $this->Subject->id = $id;
            try
            {
                foreach($this->data['Subject'] as $key => $value)
                {
                    if ($this->Subject->save($this->request->data['Subject'][$key]))
                    {
                        $this->Session->setFlash('Your Subject has been updated.','flash',array('alert'=>'success'));                        
                    }
                }
                return $this->redirect(array('action' => 'index'));
            }
            catch (Exception $e)
            {
                $this->Session->setFlash('Subject Name already exist.','flash',array('alert'=>'danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }
        if (!$this->request->data)
        {
            $this->request->data = $post;
        }
    }    
    public function deleteall()
    {
        if ($this->request->is('post'))
        {
            try
            {
                foreach($this->data['Subject']['id'] as $key => $value)
                {
                    $this->Subject->delete($value);
                }
                $this->Session->setFlash('Your Subject has been deleted.','flash',array('alert'=>'success'));
            }
            catch (Exception $e)
            {
                $this->Session->setFlash('Delete exam first!','flash',array('alert'=>'danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }        
        $this->redirect(array('action' => 'index'));
    }
}
