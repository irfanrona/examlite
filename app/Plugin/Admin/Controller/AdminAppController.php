<?php
App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');

class AdminAppController extends AppController
{
    public $helpers = array('Html', 'Form','Session','Paginator');
    public $components = array('Session','Paginator');
    function authenticate()
    {
        // Check if the session variable User exists, redirect to loginform if not
        if(!$this->Session->check('User'))
        {
            $this->redirect(array('controller' => 'users', 'action' => 'login_form'));
            exit();
        }
    } 
    public function beforeFilter()
    {
        parent::beforeFilter();
        $currAction=strtolower($this->action);
        $currController=strtolower($this->params['controller']);
        if($currAction!='login_form' && $currController!='forgots')
        {
            $this->authenticate();            
        }
        if($currAction!='login_form' && $currAction!='myprofile' && $currController!='eldialogs' && $currAction!='changepass' && $currAction!='logout' && $currController!='forgots')
        {
            $this->userPermission();
        }
        $this->set('menuArr',$this->userMenu());
    }
    public function userPermission()
    {
        $this->loadModel('Page');
        $isPermission=true;
        $UserArr=$this->Session->read('User');
        if($UserArr['User']['ugroup_id']!=1)
        {
            $userPermissionArr=$this->Page->find('first',array('joins'=>array(array
                                                                       ('table'=>'page_rights','alias'=>'PageRight','type'=>'Inner',
                                                                        'conditions'=>array('Page.id=PageRight.page_id'))),
                                                        'conditions'=>array('PageRight.ugroup_id'=>$UserArr['User']['ugroup_id'],'LOWER(Page.controller_name)'=>strtolower($this->params['controller'])),
                                                        'fields'=>array('Page.*','PageRight.*')));
            if(!isset($userPermissionArr['PageRight']['view_right']) || $userPermissionArr['PageRight']['view_right']==0)
            $isPermission=false;
            if($isPermission==false)
            {
                $this->Session->setFlash('No Permission!','flash',array('alert'=>'danger'));
                $this->redirect(array('controller'=>'admin','action' => 'index'));
            }
        }
    }
    public function userMenu()
    {
        $UserArr=$this->Session->read('User');
        $this->loadModel('Page');
        if($UserArr['User']['ugroup_id']==1)
        {
            $menuArr=$this->Page->find('all',array('conditions'=>array('parent_id'=>0),'order'=>array('ordering'=>'asc')));
        }
        else
        {
            $menuArr=$this->Page->find('all',array('joins'=>array(array
                                                                   ('table'=>'page_rights','alias'=>'PageRight','type'=>'Inner',
                                                                    'conditions'=>array('Page.id=PageRight.page_id'))),
                                                    'conditions'=>array('PageRight.ugroup_id'=>$UserArr['User']['ugroup_id'],'parent_id'=>0),
                                                    'order'=>array('Page.ordering'=>'asc')));
        }
        return$menuArr;
    }
}
