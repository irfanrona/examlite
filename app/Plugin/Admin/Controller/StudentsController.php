<?php
App::uses('CakeTime', 'Utility');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
class StudentsController extends AdminAppController {
    public $helpers = array('Html', 'Form','Session','Paginator');
    public $components = array('Session','Paginator','search-master.Prg','CustomFunction');
    public $presetVars = true;
    var $paginate = array('limit'=>20,'maxLimit'=>500,'page'=>1,'order'=>array('Student.group_name'=>'asc'));
    var $paginate1 = array('limit'=>20,'maxLimit'=>500,'page'=>1,'order'=>array('Wallet.id'=>'desc'));
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->currentDateTime=CakeTime::format('Y-m-d H:i:s',CakeTime::convert(time(),$this->siteTimezone));
        $this->adminId=$this->adminValue['User']['id'];        
    }
    public function index()
    {
        $this->Prg->commonProcess();
        $this->Paginator->settings = $this->paginate;
        $this->Paginator->settings['conditions'] = array($this->Student->parseCriteria($this->Prg->parsedParams()));
        $this->set('Student', $this->Paginator->paginate());
        $this->set('frontExamPaid',$this->frontExamPaid);
    }    
    public function add()
    {
        $this->loadModel('Group');
        $this->set('group_id', $this->Group->find('list',array('fields'=>array('id','group_name'))));
        if ($this->request->is('post'))
        {
            try
            {
                $passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
                $this->request->data['Student']['password'] = $passwordHasher->hash($this->request->data['Student']['password']);
                $this->request->data['Student']['reg_status'] = "Done";
                if ($this->Student->save($this->request->data))
                {
                    $this->loadModel('StudentGroup');
                    $this->request->data['StudentGroup']['student_id'] = $this->Student->id;
                    if(is_array($this->request->data['StudentGroup']['group_name']))
                    {
                        foreach($this->request->data['StudentGroup']['group_name'] as $key => $value)
                        {
                            $this->StudentGroup->create();
                            $this->request->data['StudentGroup']['group_id']=$value;
                            $this->StudentGroup->save($this->request->data);                        
                        }
                    }
                    $this->Session->setFlash('Your Student has been saved.','flash',array('alert'=>'success'));
                    return $this->redirect(array('action' => 'index'));
                }
                $this->set('isError',true);
            }
            catch (Exception $e)
            {
                $this->Session->setFlash('Student Email already exist.','flash',array('alert'=>'danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }
        else
        {
            $this->layout = null;
            $this->set('isError',false);
        }
    }
    public function edit($id = null)
    {
        $this->layout = null;
        $this->loadModel('Group');
        $this->set('group_id', $this->Group->find('list',array('fields'=>array('id','group_name'))));
        if (!$id)
        {
            throw new NotFoundException(__('Invalid post'));
        }
        $ids=explode(",",$id);
        $post=array();
        $group_select=array();
        foreach($ids as $id)
        {
            $post[]=$this->Student->findByid($id);            
        }
        $this->set('Student',$post);
        if (!$post)
        {
            throw new NotFoundException(__('Invalid post'));
        }
        if ($this->request->is(array('post', 'put')))
        {
            $this->Student->id = $id;
            try
            {
                foreach($this->data['Student'] as $key => $value)
                {
                    if($this->request->data['Student'][$key]['status']=="Active")
                    {
                        $this->request->data['Student'][$key]['reg_status']="Done";
                        $this->request->data['Student'][$key]['reg_code']="";
                    }
                    if ($this->Student->save($this->request->data['Student'][$key]))
                    {
                        $this->loadModel('StudentGroup');
                        $this->StudentGroup->deleteAll(array('StudentGroup.student_id'=>$key));
                        if(is_array($this->request->data['StudentGroup'][$key]['group_name']))
                        {
                            foreach($this->request->data['StudentGroup'][$key]['group_name'] as $key1 => $value1)
                            {
                                $this->StudentGroup->create();
                                $this->request->data['StudentGroup'][$key1]['student_id']=$key;
                                $this->request->data['StudentGroup'][$key1]['group_id']=$value1;                            
                                $this->StudentGroup->save($this->request->data['StudentGroup'][$key1]);
                            }
                        }
                        $this->Session->setFlash('Your Student has been updated.','flash',array('alert'=>'success'));                        
                    }
                }                
                return $this->redirect(array('action' => 'index'));
            }
            catch (Exception $e)
            {
                $this->Session->setFlash('Student Email already exist.','flash',array('alert'=>'danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }
        if (!$this->request->data)
        {
            $this->request->data = $post;
        }
    }    
    public function deleteall()
    {
        if ($this->request->is('post'))
        {
            foreach($this->data['Student']['id'] as $key => $value)
            {
                $this->Student->delete($value);
            }
            $this->Session->setFlash('Your Student has been deleted.','flash',array('alert'=>'success'));
        }        
        $this->redirect(array('action' => 'index'));
    }
    public function view($id = null)
    {
        $this->layout = null;
        if (!$id)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        $post = $this->Student->findById($id);
        if (!$post)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        if(strlen($post['Student']['photo'])>0)
        $std_img='student_thumb/'.$post['Student']['photo'];
        else
        $std_img='User.png';
        $this->set('post', $post);
        $this->set('std_img', $std_img);
        $this->set('id', $id);
    }
    public function changePass($id = null)
    {
        $this->layout = null;
        if (!$id)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        $post = $this->Student->findById($id);
        if (!$post)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->request->is(array('post', 'put')))
        {
            $passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
            $this->Student->id = $id;
            $this->request->data['Student']['password'] = $passwordHasher->hash($this->request->data['Student']['password']);
            if ($this->Student->save($this->request->data))
            {
                $this->Session->setFlash('Password Changed Successfully','flash',array('alert'=>'success'));
                $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        if (!$this->request->data)
        {
            $this->request->data = $post;
        }
    }
    public function changePhoto($id = null)
    {
        $this->layout = null;
        if (!$id)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        $post = $this->Student->findById($id);
        if (!$post)
        {
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->request->is(array('post', 'put')))
        {
            $this->Student->id = $id;
            if ($this->Student->save($this->request->data))
            {
                $this->Session->setFlash('Photo Changed Successfully','flash',array('alert'=>'success'));
                $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash('Invalid Post.','flash',array('alert'=>'danger'));
            $this->redirect(array('action' => 'index'));
        }
        if (!$this->request->data)
        {
            $this->request->data = $post;
        }
    }
    public function wallet($id = null)
    {
        $this->layout = null;
        $this->loadModel('Wallet');
        $ids=explode(",",$id);
        $post=array();
        foreach($ids as $id)
        {
            $post[]=$this->Student->find('first',array('joins' =>array(array('table'=>'wallets','alias'=>'Wallet','type'=>'Left','conditions'=>array('Student.id=Wallet.student_id'))),
                                                     'conditions'=>array('Student.id'=>$id),
                                                     'fields'=>array("id","email","name","phone","Wallet.balance"),
                                                     'order'=>array('Student.id ASC','Wallet.id DESC')));   
        }
        $this->set('Student',$post);
        if (!$post)
        {
            throw new NotFoundException(__('Invalid post'));
        }
        if ($this->request->is(array('post', 'put')))
        {
            $this->Student->id = $id;
            try
            {
                foreach($this->data['Student'] as $key => $value)
                {
                    if($this->CustomFunction->WalletInsert($this->request->data['Student'][$key]['id'],$this->request->data['Student'][$key]['amount'],$this->request->data['Student'][$key]['action'],$this->currentDateTime,"AD",$this->request->data['Student'][$key]['remarks'],$this->adminId))
                    {
                        $this->Session->setFlash('Student Wallet has been updated.','flash',array('alert'=>'success'));
                    }
                    else
                    {
                        $this->Session->setFlash('Invalid Amount.','flash',array('alert'=>'danger'));
                    }
                }                
                return $this->redirect(array('action' => 'index'));
            }
            catch (Exception $e)
            {
                $this->Session->setFlash('Student record not found.','flash',array('alert'=>'danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }        
    }
    public function trnhistory()
    {
        $this->Prg->commonProcess();
        $this->Paginator->settings = $this->paginate1;
        $this->Paginator->settings['conditions'] = array($this->Student->parseCriteria($this->Prg->parsedParams()));
        $this->Paginator->settings['joins'] = array(array('table'=>'wallets','alias'=>'Wallet','type'=>'Inner','conditions'=>array('Student.id=Wallet.student_id')));
        $this->Student->virtualFields = array('id' => 'Wallet.id','in_amount' => 'Wallet.in_amount','out_amount' => 'Wallet.out_amount','balance' => 'Wallet.balance',
                                              'date' => 'Wallet.date','type' => 'Wallet.type','remarks' => 'Wallet.remarks');
        $this->Paginator->settings['fields'] = array("email","Wallet.in_amount","Wallet.out_amount","Wallet.balance","Wallet.date","Wallet.type","Wallet.remarks");
        $this->Paginator->settings['order'] = array('id'=>'desc');
        $payment_type_arr=array("AD"=>"Administrator","PG"=>"Payment Gateway","EM"=>"Pay Exam");
        $this->set('payment_type_arr',$payment_type_arr);
        $this->set('Transactionhistory', $this->Paginator->paginate());
    }
}