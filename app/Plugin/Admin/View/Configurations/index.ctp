<?php echo $this->Session->flash();?>
<div class="row">
    <div class="col-md-12">    
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="widget">
                    <h4 class="widget-title">Configuration<span> Options</span></h4>
                </div>
            </div>
                <div class="panel-body">
                <p align="right"><?php echo $this->Html->link('<span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Configure Payment Setting',array('controller' => 'Payments','action'=>'index'),array('escape' => false,'class'=>'btn btn-info disabled'));?></p>
                <?php echo $this->Form->create('Configuration', array( 'controller' => 'Configurations', 'action' => 'index','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal'));?>
                    <div class="form-group">
                        <label for="site_name" class="col-sm-2 control-label">Site Name</label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('name',array('label' => false,'class'=>'form-control input-sm validate[required]','data-errormessage'=>'Site Name Required','placeholder'=>'Site Name','div'=>false));?>
                        </div>
                         <label for="site_name" class="col-sm-2 control-label">Organization Name</label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('organization_name',array('label' => false,'class'=>'form-control input-sm validate[required]','data-errormessage'=>'Organization Name Required','placeholder'=>'Organization Name','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="site_name" class="col-sm-2 control-label">Domain Name</label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('domain_name',array('label' => false,'class'=>'form-control input-sm validate[required]','data-errormessage'=>'Domain Name Required','placeholder'=>'Domain Name','div'=>false));?>
                        </div>
                        <label for="site_name" class="col-sm-2 control-label">Organization Email</label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('email',array('label' => false,'class'=>'form-control input-sm validate[required]','data-errormessage'=>'Email Required','placeholder'=>'Organization Email','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="site_name" class="col-sm-2 control-label">Meta Title</label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('meta_title',array('label' => false,'class'=>'form-control input-sm','placeholder'=>'Meta Data','div'=>false));?>
                        </div>
                        <label for="site_name" class="col-sm-2 control-label">Meta Description</label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('meta_desc',array('label' => false,'class'=>'form-control input-sm','placeholder'=>'Meta Description','div'=>false));?>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="site_name" class="col-sm-2 control-label">Time Zone</label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->select('timezone',$timezones,array('empty'=>'Please Select Timezone','label' => false,'class'=>'form-control input-sm validate[required]','div'=>false));?>
                        </div>
                        <label for="site_name" class="col-sm-2 control-label">Header Contact</label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('contact',array('label' => false,'class'=>'form-control input-sm','placeholder'=>'Header Contact','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="site_name" class="col-sm-3 control-label">Enable SMS Notification</label>
                        <div class="col-sm-1">
                           <?php echo $this->Form->checkbox('sms',array('label' => false,'class'=>'form-control','div'=>false));?>
                        </div>
                         <label for="site_name" class="col-sm-3 control-label">Enable Email Notification</label>
                        <div class="col-sm-1">
                           <?php echo $this->Form->checkbox('email_notification',array('label' => false,'class'=>'form-control','div'=>false));?>
                        </div>
                         <label for="site_name" class="col-sm-3 control-label">Guest Login</label>
                        <div class="col-sm-1">
                           <?php echo $this->Form->checkbox('guest_login',array('label' => false,'class'=>'form-control','div'=>false));?>
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label for="site_name" class="col-sm-3 control-label">Front End Registration</label>
                        <div class="col-sm-1">
                           <?php echo $this->Form->checkbox('front_end',array('label' => false,'class'=>'form-control','div'=>false));?>
                        </div>
                        <label for="site_name" class="col-sm-3 control-label">Front End Slides</label>
                        <div class="col-sm-1">
                           <?php echo $this->Form->checkbox('slides',array('label' => false,'class'=>'form-control','div'=>false));?>
                        </div>
                        <label for="site_name" class="col-sm-3 control-label">Tranlatation</label>
                        <div class="col-sm-1">
                           <?php echo $this->Form->checkbox('translate',array('label' => false,'class'=>'form-control','div'=>false));?>
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="site_name" class="col-sm-3 control-label">Paid Exam</label>
                        <div class="col-sm-1">
                           <?php echo $this->Form->checkbox('paid_exam',array('label' => false,'class'=>'form-control','div'=>false));?>
                        </div>
                        <label for="site_name" class="col-sm-3 control-label">Leader Board</label>
                        <div class="col-sm-1">                            
                           <?php echo $this->Form->checkbox('leader_board',array('label' => false,'class'=>'form-control','div'=>false));?>
                        </div>           
                    </div>
                    <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-refresh"></span>&nbsp;Save Settings</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>