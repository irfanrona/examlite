<div class="row">
    <div class="col-md-12">
        <div class="btn-group">
            <?php $url=$this->Html->url(array('controller'=>'Exams'));
            echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>&nbsp;Add New Exam',array('controller'=>'Exams','action'=>'add'),array('escape'=>false,'class'=>'btn btn-success'));
            echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>&nbsp;Edit','#',array('name'=>'editallfrm','id'=>'editallfrm','onclick'=>"check_perform_editfull('$url');",'escape'=>false,'class'=>'btn btn-warning'));
            echo $this->Html->link('<span class="glyphicon glyphicon-trash"></span>&nbsp;Delete','#',array('name'=>'deleteallfrm','id'=>'deleteallfrm','onclick'=>'check_perform_delete();','escape'=>false,'class'=>'btn btn-danger'));?>
        </div>
    </div>
        <?php echo $this->element('pagination');
        $page_params = $this->Paginator->params();
        $limit = $page_params['limit'];
        $page = $page_params['page'];
        $serial_no = 1*$limit*($page-1)+1;?>
        <?php echo $this->Form->create(array('name'=>'deleteallfrm','action' => 'deleteall'));?>
</div>
<?php echo $this->Session->flash();?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="widget">
                    <h4 class="widget-title"> <span>Exams</span></h4>
                </div>
            </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th><?php echo $this->Form->checkbox('checkbox', array('value'=>'deleteall','name'=>'selectAll','label'=>false,'id'=>'selectAll','hiddenField'=>false));?></th>
                            <th><?php echo $this->Paginator->sort('id', 'S.No.', array('direction' => 'desc'));?></th>
                            <th><?php echo $this->Paginator->sort('name', 'Exam Name', array('direction' => 'asc'));?></th>
                            <th><?php echo $this->Paginator->sort('start_date', 'Date', array('direction' => 'asc'));?></th>
                            <th>Exam Groups</th>
                            <th><?php echo $this->Paginator->sort('duration', 'Duration', array('direction' => 'asc'));?></th>
                            <th><?php echo $this->Paginator->sort('status', 'Status', array('direction' => 'asc'));?></th>
                            <th>View</th>
                            <th>Action</th>
                        </tr>
                        <?php foreach ($Exam as $post):
                        $id=$post['Exam']['id'];?>
                        <tr>
                            <td><?php echo $this->Form->checkbox(false,array('value' => $post['Exam']['id'],'name'=>'data[Exam][id][]','id'=>"DeleteCheckbox$id",'class'=>'chkselect'));?></td>
                            <td><?php echo $serial_no++; ?></td>
                            <td><?php echo h($post['Exam']['name']); ?></td>
                            <td><?php echo $this->Time->format('jS F Y',$post['Exam']['start_date']);?> to <?php echo$this->Time->format('jS F Y',$post['Exam']['end_date']);?></td>
                            <td><?php foreach($post['Group'] as $k=>$groupName):
                            echo h($groupName['group_name']);?> |
                            <?php endforeach;unset($groupName);?></td>
                            <td><?php echo $this->Function->secondsToWords($post['Exam']['duration']*60);?></td>
                            <td><?php echo $post['Exam']['status']; ?></td>
                            <td><?php echo $this->Html->link('View','#',array('onclick'=>"show_modal('$url/View/$id');",'escape'=>false));?>
                            <td><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>&nbsp;Edit',array('controller'=>'Exams','action'=>'edit',$id),array('escape'=>false,'class'=>'btn btn-warning'));?>
                            <?php echo $this->Html->Link('<span class="glyphicon glyphicon-trash"></span>&nbsp;Delete','#',array('onclick'=>"check_perform_sdelete('$id');",'class'=>'btn btn-danger','escape'=>false));?></td>                            
                        </tr>
                        <?php endforeach; ?>
                        <?php unset($post); ?>
                        </table>
                </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end();?>
<?php echo $this->element('pagination');?>
<div class="modal fade" id="targetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-content">    
  </div>
</div>