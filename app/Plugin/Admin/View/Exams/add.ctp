<?php echo $this->Html->css('/Admin/css/bootstrap-multiselect');
echo $this->Html->css('/Admin/css/bootstrap-datetimepicker.min');
echo $this->fetch('css');
echo $this->Html->script('/Admin/js/bootstrap-multiselect');
echo $this->Html->script('/Admin/js/moment');
echo $this->Html->script('/Admin/js/bootstrap-datetimepicker.min');
echo $this->fetch('script');?>
<script type="text/javascript">
    $(document).ready(function(){        
        $('#post_req').validationEngine();
        $('.multiselectgrp').multiselect();
         $('#start_date').datetimepicker();
            $('#end_date').datetimepicker();
	    $("#start_date").on("dp.change",function (e) {var d= new Date(e.date);
               $('#end_date').data("DateTimePicker").setMinDate(d.setDate(d.getDate()-1));
            });
            $("#end_date").on("dp.change",function (e) {var d= new Date(e.date);
               $('#start_date').data("DateTimePicker").setMaxDate(d.setDate(d.getDate()+0));
            });
});
</script>
<div class="row">
<?php echo $this->Session->flash();?>
    <div class="col-md-12">      
        <div class="panel panel-default">
            <div class="panel-heading">
		<div class="widget">
		    <h4 class="widget-title">Add <span>Exams</span></h4>
		</div>
	    </div>
                <div class="panel-body">
                <?php echo $this->Form->create('Exam', array( 'controller' => 'Exam', 'action' => 'add','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal'));?>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-2 control-label"><small>Exam Name</small></label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('name',array('label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Exam Name','div'=>false));?>
                        </div>                    
                        <label for="group_name" class="col-sm-2 control-label"><small>Passing Percent</small></label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('passing_percent',array('label' => false,'class'=>'form-control input-sm validate[required,custom[number]','placeholder'=>'Passing Percent','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-2 control-label"><small>Exam Instruction</small></label>
                        <div class="col-sm-10">
                           <?php echo $this->Tinymce->input('instruction', array('label' => false,'class'=>'form-control','div'=>false,'placeholder'=>'Exam Instruction'),array('language'=>'en'),'full');?>
                        </div>                        
                    </div>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-2 control-label"><small>Exam Duration (Min.)</small></label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('duration',array('label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Exam Duration (Min.)','div'=>false));?>
                        </div>                    
                        <label for="group_name" class="col-sm-2 control-label"><small>Attempt Count</small></label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('attempt_count',array('label' => false,'class'=>'form-control input-sm validate[required,custom[number]','placeholder'=>'Attempt Count','div'=>false));?>                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-2 control-label"><small>Start Date</small></label>
                        <div class="col-sm-4">
						<div class="input-group date" id="start_date" data-date-format="YYYY-MM-DD H:m:s">                        
                            <?php echo $this->Form->input('start_date',array('type'=>'text','readonly'=>'readonly','label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Start Date','div'=>false));?>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                        </div>
						</div>
                        <label for="group_name" class="col-sm-2 control-label"><small>End Date</small></label>
                        <div class="col-sm-4">
						<div class="input-group date" id="end_date" data-date-format="YYYY-MM-DD H:m:s">
                           <?php echo $this->Form->input('end_date',array('type'=>'text','id'=>'end_date','readonly'=>'readonly','label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'End Date','div'=>false));?>
                           <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                        </div>
						</div>
                    </div>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-2 control-label"><small>Declare Result</small></label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('declare_result',array('type'=>'radio','options'=>array("Yes"=>"Yes","No"=>"No"),'default'=>'Yes','legend'=>false,'before' => '<label class="radio-inline">','separator' => '</label><label class="radio-inline">','label' => false,'div'=>false));?>
                        </div>                    
                        <label for="group_name" class="col-sm-2 control-label"><small>Select Group</small></label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->select('ExamGroup.group_name',$group_id,array('multiple'=>true,'label' => false,'class'=>'form-control multiselectgrp validate[required]','placeholder'=>'Student Group','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-2 control-label"><small>Negative Marking</small></label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('negative_marking',array('type'=>'radio','options'=>array("Yes"=>"Yes","No"=>"No"),'default'=>'Yes','legend'=>false,'before' => '<label class="radio-inline">','separator' => '</label><label class="radio-inline">','label' => false,'div'=>false));?>
                        </div>	    
                        <label for="group_name" class="col-sm-2 control-label"><small>Random Question</small></label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('ques_random',array('type'=>'radio','options'=>array("1"=>"Yes","0"=>"No"),'default'=>'0','legend'=>false,'before' => '<label class="radio-inline">','separator' => '</label><label class="radio-inline">','label' => false,'div'=>false));?>			   
                        </div>                                                                               
                    </div>
		    <div class="form-group">
                        <label for="group_name" class="col-sm-2 control-label"><small>Result After Finish</small></label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('finish_result',array('type'=>'radio','options'=>array("1"=>"Yes","0"=>"No"),'default'=>'0','legend'=>false,'before' => '<label class="radio-inline">','separator' => '</label><label class="radio-inline">','label' => false,'div'=>false));?>
			</div>	    
                        <div class="col-sm-2">                           
                        </div>                                                                               
                    </div>
		    <?php if($frontExamPaid>0){?>
		     <div class="form-group">
                        <label for="group_name" class="col-sm-2 control-label"><small>Paid Exam</small></label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('paid_exam',array('type'=>'radio','options'=>array("1"=>"Yes",""=>"No"),'default'=>'','legend'=>false,'before' => '<label class="radio-inline">','separator' => '</label><label class="radio-inline">','label' => false,'div'=>false));?>
                        </div>	    
                        <label for="group_name" class="col-sm-2 control-label"><small>Amount</small></label>
                        <div class="col-sm-4">
                           <?php echo $this->Form->input('amount',array('label' => false,'class'=>'form-control input-sm validate[custom[number],min[1],condRequired[ExamPaidExam1]]','placeholder'=>'Amount','div'=>false));?>
                        </div>                                                                               
                    </div>
		    <?php }?>
                    <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign"></span> Save</button>
                            <button type="button" class="btn btn-danger" onclick="window.location='index'"><span class="glyphicon glyphicon-remove"></span> Close</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>