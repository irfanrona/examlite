<div class="col-md-5 login">
<?php echo $this->Session->flash();?>
			<?php echo $this->Form->create('User', array( 'controller' => 'Users', 'action' => 'login_form','name'=>'post_req','id'=>'post_req'));?>
				<div class="form-group">
					<div class="row">
						<label for="admin_id" class="col-sm-12 control-label">Admin / Teacher ID :</label>
					</div>
					<div class="input-group">
						<span class="input-group-addon glyphicon glyphicon-user"></span>
                                                <?php echo $this->Form->input('username',array('label' => false,'class'=>'form-control validate[required]','placeholder'=>'Admin / Teacher ID','div'=>false));?>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<label for="pass" class="col-sm-12 control-label">Password :</label>
					</div>
					<div class="input-group">
						<span class="input-group-addon glyphicon glyphicon-lock"></span>
						<?php echo $this->Form->input('password',array('label' => false,'class'=>'form-control validate[required,minSize[4],maxSize[15]]','placeholder'=>'Password','div'=>false));?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<p class="help-block"><?php echo$this->Html->link('Forgot Password',array('controller'=>'Forgots','action'=>'password'));?></p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<p class="help-block"><?php echo$this->Html->link('Forgot User Name',array('controller'=>'Forgots','action'=>'username'));?></p>
					</div>
				</div>
				<div class="form-group col-md-12">
					<div class="row">                                        
						<button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-ok"></span>&nbsp;Submit</button>
					</div>
				</div>
                        </form>
		</div>
