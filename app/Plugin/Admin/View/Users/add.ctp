<script type="text/javascript">
    $(document).ready(function(){
        $('#post_req').validationEngine();
        });
</script>
<div <?php if(!$isError){?> class="container"<?php }?>>
<div class="row">
<?php echo $this->Session->flash();?>
    <div class="col-md-12">    
        <div class="panel panel-default mrg">
            <div class="panel-heading"><div class="widget-modal"><h4 class="widget-modal-title">Add <span>Users</span><?php if(!$isError){?><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><?php }?></strong></h4></div></div>
                <div class="panel-body">
                <?php echo $this->Form->create('User', array( 'controller' => 'User', 'action' => 'add','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal'));?>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label">Group</label>
                        <div class="col-sm-9">
                            <?php echo $this->Form->select('ugroup_id',$ugroup,array('empty'=>null,'label' => false,'class'=>'form-control input-sm','div'=>false));?>
                            </div>
                    </div>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label"><small>Username</small></label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('username',array('label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'User Name','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label"><small>Password</small></label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->password('password',array('label' => false,'class'=>'form-control input-sm validate[required,minSize[4],maxSize[15]]','placeholder'=>'Password','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label"><small>Name</small></label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('name',array('label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Name','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label"><small>Email</small></label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('email',array('label' => false,'class'=>'form-control input-sm validate[required,custom[email]]','placeholder'=>'Email','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label"><small>Mobile</small></label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('mobile',array('label' => false,'class'=>'form-control input-sm validate[required,custom[onlyNumberSp],minSize[10],maxSize[10]]','placeholder'=>'Mobile','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign"></span> Save</button>
                            <?php if(!$isError){?><button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Close</button><?php }?>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>