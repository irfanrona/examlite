<?php echo $this->Html->css('/Admin/css/bootstrap-multiselect');
echo $this->fetch('css');
echo $this->Html->script('/Admin/js/bootstrap-multiselect');
echo $this->fetch('script');?>
<script type="text/javascript">
    $(document).ready(function(){
    $('#post_req').validationEngine();
    $('.multiselect').multiselect();
});
</script>
<div class="container">
<div class="row">
<?php echo $this->Session->flash();?>
    <div class="col-md-12">    
        <div class="panel panel-default mrg">
	    <div class="panel-heading"><div class="widget-modal"><h4 class="widget-modal-title">Edit <span>Students</span><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></h4></div></div>            
                <div class="panel-body">
                <?php echo $this->Form->create('Student', array( 'controller' => 'Student','name'=>'post_req','id'=>'post_req'));?>
                <?php foreach ($Student as $k=>$post): $id=$post['Student']['id'];$form_no=$k+1;?>
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong class="text-danger"><small>Form <?php echo$form_no?></small></strong></div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">	
									<div class="form-group">
										<label for="group_name"><small>Email<span class="text-danger"> *</span></small></label>
										<?php echo $this->Form->input('email',array('name'=>"data[Student][$id][email]",'value'=>$post['Student']['email'],'label' => false,'class'=>'form-control input-sm validate[required,custom[email]]','placeholder'=>'Email','div'=>false));?>
									</div>
								</div>	
								<div class="col-md-6">	
									<div class="form-group">
										<label for="group_name"><small>Student Group<span class="text-danger"> *</span></small></label></br/>
											<?php $gp2=array();
                                                                                        foreach($post['Group'] as $groupName):
                                                                                $gp2[]= $groupName['id'];?>
                                                                                <?php endforeach;unset($groupName);?>
										   <?php echo $this->Form->select('StudentGroup.group_name',$group_id,array('name'=>"data[StudentGroup][$id][group_name]",'value'=>$gp2,'multiple'=>true,'label' => false,'class'=>'form-control multiselect','div'=>false));$gp2=array();?>
									</div>
								</div>	
								<div class="col-md-6">	
									<div class="form-group">
										<label for="group_name"><small>Name<span class="text-danger"> *</span></small></label>
										<?php echo $this->Form->input('name',array('name'=>"data[Student][$id][name]",'value'=>$post['Student']['name'],'label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Name','div'=>false));?>
									</div>
								</div>	
								<div class="col-md-6">
									<div class="form-group">
										<label for="group_name"><small>Address<span class="text-danger"> *</span></small></label>
										<?php echo $this->Form->input('address',array('name'=>"data[Student][$id][address]",'value'=>$post['Student']['address'],'label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Address','div'=>false));?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="group_name"><small>Phone<span class="text-danger"> *</span></small></label>
										<?php echo $this->Form->input('phone',array('name'=>"data[Student][$id][phone]",'value'=>$post['Student']['phone'],'label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Phone','div'=>false));?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="group_name"><small>Guardian Phone</small></label>
										<?php echo $this->Form->input('guardian_phone',array('name'=>"data[Student][$id][guardian_phone]",'value'=>$post['Student']['guardian_phone'],'label' => false,'class'=>'form-control input-sm','placeholder'=>'Guardian Phone','div'=>false));?>
									</div>
								</div>
								<div class="col-md-6">	
									<div class="form-group">
										<label for="group_name"><small>Enrolment Number</small></label>
										<?php echo $this->Form->input('enroll',array('name'=>"data[Student][$id][enroll]",'value'=>$post['Student']['enroll'],'label' => false,'class'=>'form-control input-sm','placeholder'=>'Enrolment Number','div'=>false));?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="group_name"><small>Status</small></label>
										<?php echo $this->Form->select('status',array("Active"=>"Active","Pending"=>"Pending","Suspend"=>"Suspend"),array('name'=>"data[Student][$id][status]",'value'=>$post['Student']['status'],'empty'=>null,'label' => false,'class'=>'form-control input-sm validate[required]','div'=>false));?>
									</div>
								</div>	
								<div class="form-group text-left">
									<div class="col-sm-offset-3 col-sm-10">
										<?php echo $this->Form->input('id', array('name'=>"data[Student][$id][id]",'value'=>$post['Student']['id'],'type' => 'hidden'));?>                            
									</div>
								</div> 
							</div>
						</div>
					</div>				
                    <?php endforeach; ?>
                        <?php unset($post); ?>
                        <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">                            
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-refresh"></span> Update</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>