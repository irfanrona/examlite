<div class="row">
    <div class="col-md-12">
        <div class="btn-group">
            <?php $url=$this->Html->url(array('controller'=>'Students')); echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>&nbsp;Add New Student','#',array('name'=>'add','id'=>'add','onclick'=>"check_perform_add('$url/add');",'escape'=>false,'class'=>'btn btn-success'));?>
            <?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>&nbsp;Edit','#',array('name'=>'editallfrm','id'=>'editallfrm','onclick'=>"check_perform_edit('$url');",'escape'=>false,'class'=>'btn btn-warning'));?>
            <?php echo $this->Html->link('<span class="glyphicon glyphicon-trash"></span>&nbsp;Delete','#',array('name'=>'deleteallfrm','id'=>'deleteallfrm','onclick'=>'check_perform_delete();','escape'=>false,'class'=>'btn btn-danger'));?>
            <?php if($frontExamPaid>0){echo $this->Html->link('<span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Wallet','#',array('name'=>'walletallfrm','id'=>'walletallfrm','onclick'=>"check_perform_select('$url','wallet');",'escape'=>false,'class'=>'btn btn-info disabled'));
            echo $this->Html->link('<span class="glyphicon glyphicon-briefcase"></span>&nbsp;Transaction History',array('controller'=>'Students','action'=>'trnhistory'),array('escape'=>false,'class'=>'btn btn-info disabled'));}?>
        </div>
    </div>
        <?php echo $this->element('pagination');
        $page_params = $this->Paginator->params();
        $limit = $page_params['limit'];
        $page = $page_params['page'];
        $serial_no = 1*$limit*($page-1)+1;?>
        <?php echo $this->Form->create(array('name'=>'deleteallfrm','action' => 'deleteall'));?>
</div>
<?php echo $this->Session->flash();?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title"> <span>Students</span></h4>
			</div>
		</div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th><?php echo $this->Form->checkbox('checkbox', array('value'=>'deleteall','name'=>'selectAll','label'=>false,'id'=>'selectAll','hiddenField'=>false));?></th>
                            <th><?php echo $this->Paginator->sort('id', 'S.No.', array('direction' => 'desc'));?></th>
                            <th><?php echo $this->Paginator->sort('email', 'Email', array('direction' => 'asc'));?></th>
                            <th><?php echo $this->Paginator->sort('enroll', 'Enrolment Number', array('direction' => 'asc'));?></th>
                            <th><?php echo $this->Paginator->sort('phone', 'Phone', array('direction' => 'asc'));?></th>
                            <th>Student Groups</th>
                            <th><?php echo $this->Paginator->sort('created', 'Date of Admission', array('direction' => 'asc'));?></th>
                            <th><?php echo $this->Paginator->sort('status', 'Status', array('direction' => 'asc'));?></th>
                            <th>&nbsp;</th>
                            <th>Action</th>                            
                        </tr>
                        <?php foreach ($Student as $post):
                        $id=$post['Student']['id'];
                        ?>
                        <tr>
                            <td><?php echo $this->Form->checkbox(false,array('value' => $post['Student']['id'],'name'=>'data[Student][id][]','id'=>"DeleteCheckbox$id",'class'=>'chkselect'));?></td>
                            <td><?php echo $serial_no++; ?></td>
                            <td  <?php echo($post['Student']['reg_status']=="Live") ? "class=\"danger\"" : "";?>><?php echo h($post['Student']['email']); ?></td>
                            <td><?php echo h($post['Student']['enroll']); ?></td>
                            <td><?php echo h($post['Student']['phone']); ?></td>
                            <td><?php foreach($post['Group'] as $groupName):
                            echo h($groupName['group_name']);?> |
                            <?php endforeach;unset($groupName);?></td>
                            <td><?php echo $this->Time->format('d-m-Y',$post['Student']['created']); ?></td>
                            <td><?php echo $post['Student']['status']; ?></td>
                            <td><?php echo $this->Html->link('View','#',array('onclick'=>"show_modal('$url/View/$id');",'escape'=>false));?>
                            <td><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>&nbsp;Edit','#',array('name'=>'editallfrm','onclick'=>"check_perform_sedit('$url','$id');",'escape'=>false,'class'=>'btn btn-warning'));?>
                            <?php echo $this->Html->Link('<span class="glyphicon glyphicon-trash"></span>&nbsp;Delete','#',array('onclick'=>"check_perform_sdelete('$id');",'class'=>'btn btn-danger','escape'=>false));?></td>                            
                        </tr>
                        <?php endforeach; ?>
                        <?php unset($post); ?>
                        </table>
                </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end();?>
<?php echo $this->element('pagination');?>
<div class="modal fade" id="targetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">    
</div>