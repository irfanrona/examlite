	<div class="container">
		<div class="row">
			<?php $passUrl=$this->Html->url(array('controller'=>'Students','action'=>'changePass',$id));
                        $photoUrl=$this->Html->url(array('controller'=>'Students','action'=>'changePhoto',$id));
                        echo $this->Session->flash();?>
				<div class="panel panel-default mrg">
					<div class="panel-heading"><div class="widget-modal"><h4 class="widget-modal-title">View <span>Student Information</span><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></h4></div></div>
					<div class="panel-body">
						<div class="col-md-2 text-center">
							<p><div class="img-thumbnail"><?php echo $this->Html->image($std_img, array('alt' => $post['Student']['name']));?></div></p>
							<?php echo $this->Html->link('Update Photo','#',array('onclick'=>"show_modal('$photoUrl');",'class'=>'btn btn-success btn-sm btn-block','escape'=>false)); ?>
                                                        <?php echo $this->Html->link('Change Password','#',array('onclick'=>"show_modal('$passUrl');",'class'=>'btn btn-danger btn-sm btn-block','escape'=>false)); ?>
						</div>
						<div class="col-md-10"> 
							<div class="row">
								<table class="table table-striped table-bordered">
									<tr class="text-danger">
										<td><strong><small>Full Name</small></strong></td>
										<td><strong><small>Phone Number</small></strong></td>
									</tr>
									<tr>
										<td><strong><small><?php echo h($post['Student']['name']);?></small></strong></td>
										<td><strong><small><?php echo h($post['Student']['phone']);?></small></strong></td>
									</tr>
									<tr class="text-danger">
										<td><strong><small>Registered Email</small></strong></td>
										<td><strong><small>Alternate Number</small></strong></td>
									</tr>
									<tr>
										<td><strong><small><?php echo h($post['Student']['email']);?></small></strong></td>
										<td><strong><small><?php echo h($post['Student']['guardian_phone']);?></small></strong></td>
									</tr>
									</tr>
									<tr class="text-danger">
										<td><strong><small>Enrolment No.<strong><small></td>
										<td><strong><small>Admission Date</small></strong></td>
									</tr>
									<tr>
										<td><strong><small><?php echo h($post['Student']['enroll']);?></small></strong></td>
										<td><strong><small><?php echo $this->Time->format('d-m-Y',$post['Student']['created']); ?></small></strong></td>
									</tr>
									<tr class="text-danger"> 
										<td><strong><small>Group/s <strong><small></td>
										<td></td>
									</tr>
									<tr>
										<td colspan="2"><small><strong>
                                                                                <?php foreach($post['Group'] as $k=>$groupName):?>
                                                                                (<?php echo++$k;?>) <?php echo h($groupName['group_name']);?>
                                                                                <?php endforeach;unset($groupName);unset($k);?></small></strong></td>
									</tr>
								</table>
							</div>
						</div>
					</div>	
				</div>
        </div>
    </div>
