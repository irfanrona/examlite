<script type="text/javascript">
    $(document).ready(function(){
        $('#tf').hide();
        $('#ftb').hide();
        $('#qtype_id1').prop('checked', true);
        $('#qtype_id1').click(function() {
            $('#myquestiontab').show();
            $('#tf').hide();
            $('#ftb').hide();
        });
        $('#qtype_id2').click(function() {
            $('#tf').show();
            $('#myquestiontab').hide();
            $('#ftb').hide();
        });
        $('#qtype_id3').click(function() {
            $('#ftb').show();
            $('#myquestiontab').hide();
            $('#tf').hide();
        });
        $('#qtype_id4').click(function() {
            $('#ftb').hide();
            $('#myquestiontab').hide();
            $('#tf').hide();
        });        
        });
</script>
<?php echo $this->Session->flash();?>
    <?php echo $this->Form->create('Question', array( 'controller' => 'Question', 'action' => 'add','name'=>'post_req','id'=>'post_req'));?>
    <div class="row">
    <div class="col-md-12">    
        <div class="panel panel-default">
            <div class="panel-heading">
		<div class="widget">
		    <h4 class="widget-title">Add <span>Questions</span></h4>
		</div>
	    </div>
                <div class="panel-body">
                 <div class="col-md-12">
                 <div class="row">
                 <h5><strong>Subject Type</strong></h5>
                 <div class="panel panel-default">
                 <div class="panel-body">                 
                 <div class="radio-inline">                 
                    <?php echo $this->Form->radio('qtype_id',$qtype_id,array('id'=>'qtype_id','legend'=>false,'hiddenField'=>false,'separator'=> '</div><div class="radio-inline">',
                                                                             'label'=>array('class'=>'radio-inline')));?>
                 </div>
                 </div>
                 </div>
                 </div>
                 </div>
                
                <ul class="nav nav-tabs" id="myquestiontab">
                <li class="active"><a href="#Question" data-toggle="tab">Question</a></li>                
                <li><a href="#Answer1" data-toggle="tab">Answer1</a></li>
                <li><a href="#Answer2" data-toggle="tab">Answer2</a></li>
                <li><a href="#Answer3" data-toggle="tab">Answer3</a></li>
                <li><a href="#Answer4" data-toggle="tab">Answer4</a></li>
		<li><a href="#Answer5" data-toggle="tab">Answer5</a></li>
		<li><a href="#Answer6" data-toggle="tab">Answer6</a></li>
                <li><a href="#CorrectAnswer" data-toggle="tab">Correct Answers</a></li>                
                </ul>                    
                    <div class="tab-content">
                    <div class="tab-pane active" id="Question">
                    <h4>Question</h4><hr/>                    
                    <?php echo $this->Tinymce->input('question', array('label' => false,'class'=>'form-control','div'=>false,'placeholder'=>'Question'),array('language'=>'en'),'full');?>
                    </div>
								<div class="tab-pane" id="Answer1">
									<h4>Answer1</h4><hr/>
									<?php echo $this->Tinymce->input('option1', array('label' => false,'class'=>'form-control','div'=>false,'placeholder'=>'Answer1'),array('language'=>'en'),'full');?>
								</div>
								<div class="tab-pane" id="Answer2">
									<h4>Answer2</h4><hr/>
									<?php echo $this->Tinymce->input('option2', array('label' => false,'class'=>'form-control','div'=>false,'placeholder'=>'Answer2'),array('language'=>'en'),'full');?>
								</div>
								<div class="tab-pane" id="Answer3">
									<h4>Answer3</h4><hr/>
									<?php echo $this->Tinymce->input('option3', array('label' => false,'class'=>'form-control','div'=>false,'placeholder'=>'Answer3'),array('language'=>'en'),'full');?>
								</div>
								<div class="tab-pane" id="Answer4">
									<h4>Answer4</h4><hr/>
									<?php echo $this->Tinymce->input('option4', array('label' => false,'class'=>'form-control','div'=>false,'placeholder'=>'Answer4'),array('language'=>'en'),'full');?>
								</div>
								<div class="tab-pane" id="Answer5">
									<h4>Answer5</h4><hr/>
									<?php echo $this->Tinymce->input('option5', array('label' => false,'class'=>'form-control','div'=>false,'placeholder'=>'Answer5'),array('language'=>'en'),'full');?>
								</div>
								<div class="tab-pane" id="Answer6">
									<h4>Answer6</h4><hr/>
									<?php echo $this->Tinymce->input('option6', array('label' => false,'class'=>'form-control','div'=>false,'placeholder'=>'Answer6'),array('language'=>'en'),'full');?>
								</div>
                                                                <div class="tab-pane" id="CorrectAnswer">
                                                                    <?php echo $this->Form->input('answer',array('type'=>'select','multiple' => 'checkbox','options'=>array(1=>'Answer1',2=>'Answer2',3=>'Answer3',4=>'Answer4',5=>'Answer5',6=>'Answer6')));?>
								</div>
							</div>
                                                         <div class="panel-body" id="tf">
                                                         <div class="col-md-12">
                                                         <div class="row">
                                                         <h5><strong>True/False</strong></h5>
                                                         <div class="panel panel-default">
                                                         <div class="panel-body">    
                                                        <div class="radio-inline">
                                                            <?php echo $this->Form->radio('true_false',$options=array("True"=>"True","False"=>"False"),array('legend'=>false,'hiddenField'=>false,'separator'=> '</div><div class="radio-inline">',
                                                                                                                     'label'=>array('class'=>'radio-inline')));?>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                         </div>
                                                  <div class="form-group" id="ftb">
                                                                    <?php echo $this->Form->input('fill_blank',array('class'=>'form-control','div'=>false,'label'=>'Blank Space','escape'=>false,'placeholder'=>'Blank Space'));?>
								</div>                                            
						<div class="form-group">
					    <?php echo $this->Tinymce->input('explanation', array('label'=>'Explanation','class'=>'form-control','div'=>false,'placeholder'=>'Explanation'),array('language'=>'en'),'full');?>
						</div>
                                        <div class="panel panel-default mrg pull-left">
						<div class="panel-body">                                                
								<div class="form-group">
                                                                <?php echo $this->Form->input('subject_id',array('options'=>array($subject_id),'empty'=>'(Please Select)','class'=>'form-control validate[required]','div'=>false,'label'=>'Subject'));?>
								</div>
								<div class="form-group">
                                                                    <?php echo $this->Form->input('hint', array('type'=>'text','class'=>'form-control','div'=>false,'placeholder'=>'Hint'));?>
								</div>
								<div class="col-md-4">
									<div class="row">
										<div class="form-group">
										    <?php echo $this->Form->input('marks',array('class'=>'form-control validate[required,custom[number]]','div'=>false,'placeholder'=>'Marks'));?>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
                                                                            <?php echo $this->Form->input('negative_marks',array('class'=>'form-control validate[required,custom[number]]','div'=>false,'placeholder'=>'Negative Marks'));?>
									</div>
								</div>
								
								<div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <?php echo $this->Form->input('diff_id',array('options'=>array($diff_id),'empty'=>'(Please Select)','class'=>'form-control validate[required]','div'=>false,'label'=>'Difficulty Level'));?>
                                                                    </div>
								</div>                                                               
								<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign"></span> Save</button>
                                                                <button type="button" class="btn btn-danger" onclick="window.location='index'"><span class="glyphicon glyphicon-remove"></span> Close</button>
						</div>
					</div>
                                        </div>
					</div>                   
                    
    </div>    
</div>
</form>