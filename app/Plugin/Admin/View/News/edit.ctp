<div class="row">
<?php echo $this->Session->flash();?>
    <div class="col-md-12">    
        <div class="panel panel-default">
            <div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">Edit <span>News</span></h4>
			</div>
		</div>
                <div class="panel-body">
					<?php echo $this->Form->create('News', array( 'controller' => 'News','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal'));?>
					<?php foreach ($News as $k=>$post): $id=$post['News']['id'];$form_no=$k+1;?>
						<div class="panel panel-default">
							<div class="panel-heading"><strong><small class="text-danger">Form <?php echo$form_no?></small></strong></div>
							<div class="panel-body">
								<div class="form-group">
									<label for="group_name" class="col-sm-3 control-label">News Title</label>
									<div class="col-sm-9">
									   <?php echo $this->Form->input('news_title',array('name'=>"data[News][$id][news_title]",'value'=>$post['News']['news_title'],'label' => false,'class'=>'form-control input-sm validate[required]','data-errormessage'=>'News Title Required','placeholder'=>'News Title','div'=>false));?>
									</div>
								</div>
								<div class="form-group">
									<label for="group_name" class="col-sm-3 control-label">News Description</label>
									<div class="col-sm-9">
									   <?php echo $this->Tinymce->input('news_desc',array('name'=>"data[News][$id][news_desc]",'id'=>$id,'value'=>$post['News']['news_desc'],'label' => false,'class'=>'form-control','placeholder'=>'News Description','div'=>false),array('language'=>'en'),'full');?>
									</div>                        
								</div>
								<div class="form-group">
								    <label for="group_name" class="col-sm-3 control-label">Status</label>
								    <div class="col-sm-9">
									<?php echo $this->Form->select('status',array("Active"=>"Active","Suspend"=>"Suspend"),array('name'=>"data[News][$id][status]",'value'=>$post['News']['status'],'empty'=>null,'label' => false,'class'=>'form-control input-sm validate[required]','div'=>false));?>
								    </div>
								</div>
								<div class="form-group text-left">
									<div class="col-sm-offset-3 col-sm-10">
										<?php echo $this->Form->input('id', array('name'=>"data[News][$id][id]",'value'=>$post['News']['id'],'type' => 'hidden'));?>                            
									</div>
								</div>
							</div>	
						</div>						
                    <?php endforeach; ?>
                        <?php unset($post); ?>
                        <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">                            
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-refresh"></span> Update</button>
                            <button type="button" class="btn btn-danger" onclick="window.location='../index'"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>