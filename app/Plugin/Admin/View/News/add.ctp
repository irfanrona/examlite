<div class="row">
<?php echo $this->Session->flash();?>
    <div class="col-md-12">    
        <div class="panel panel-default">
            <div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">Add <span>News</span></h4>
			</div>
		</div>
                <div class="panel-body">
                <?php echo $this->Form->create('News', array( 'controller' => 'News', 'action' => 'add','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal'));?>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label"><small>News Title</small></label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('news_title',array('label' => false,'class'=>'form-control input-sm validate[required]','data-errormessage'=>'News Title Required','placeholder'=>'News Title','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label"><small>News Description</small></label>
                        <div class="col-sm-9">
                           <?php echo $this->Tinymce->input('news_desc', array('class'=>'form-control','label' => false),array('language'=>'en'),'full');?>
                        </div>
                    </div>
                    <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign"></span> Save</button>
                            <button type="button" class="btn btn-danger" onclick="window.location='index'"><span class="glyphicon glyphicon-remove"></span> Close</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>