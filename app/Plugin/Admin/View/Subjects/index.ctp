<div class="row">
    <div class="col-md-12">
        <div class="btn-group">
            <?php $url=$this->Html->url(array('controller'=>'Subjects')); echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>&nbsp;Add New Subject','#',array('name'=>'add','id'=>'add','onclick'=>"check_perform_add('$url/add');",'escape'=>false,'class'=>'btn btn-success'));?>
            <?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>&nbsp;Edit','#',array('name'=>'editallfrm','id'=>'editallfrm','onclick'=>"check_perform_edit('$url');",'escape'=>false,'class'=>'btn btn-warning'));?>
            <?php echo $this->Html->link('<span class="glyphicon glyphicon-trash"></span>&nbsp;Delete','#',array('name'=>'deleteallfrm','id'=>'deleteallfrm','onclick'=>'check_perform_delete();','escape'=>false,'class'=>'btn btn-danger'));?>
            <?php echo $this->Html->link('<span class="glyphicon glyphicon-list-alt"></span>&nbsp;View Question',array('controller'=>'Questions','action'=>'index'),array('escape'=>false,'class'=>'btn btn-success'));?>
        </div>
    </div>
        <?php echo $this->element('pagination');
        $page_params = $this->Paginator->params();
        $limit = $page_params['limit'];
        $page = $page_params['page'];
        $serial_no = 1*$limit*($page-1)+1;?>
        <?php echo $this->Form->create(array('name'=>'deleteallfrm','action' => 'deleteall'));?>
</div>
<?php echo $this->Session->flash();?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title"> <span>Subjects</span></h4>
			</div>
		</div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th><?php echo $this->Form->checkbox('checkbox', array('value'=>'deleteall','name'=>'selectAll','label'=>false,'id'=>'selectAll','hiddenField'=>false));?></td>
                            <th><?php echo $this->Paginator->sort('id', 'S.No.', array('direction' => 'desc'));?></td>
                            <th><?php echo $this->Paginator->sort('subject_name', 'Subject Name', array('direction' => 'asc'));?></td>
                            <th>Add Question</td>
                            <th>Action</td>
                            <th>Question Bank Account</td>
                        </tr>
                        <?php foreach ($Subject as $post):
                        $id=$post['Subject']['id'];?>
                        <tr>
                            <td><?php echo $this->Form->checkbox(false,array('value' => $post['Subject']['id'],'name'=>'data[Subject][id][]','id'=>"DeleteCheckbox$id",'class'=>'chkselect'));?></td>
                            <td><?php echo $serial_no++; ?></td>
                            <td><?php echo h($post['Subject']['subject_name']); ?></td>
                            <td><?php echo $this->Html->link('<span class="glyphicon glyphicon-plus"></span>&nbsp;Add Question',array('controller'=>'Questions','action'=>'add'),array('escape'=>false,'class'=>'btn btn-success'));?></td>
                            <td><?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>&nbsp;Edit','#',array('name'=>'editallfrm','onclick'=>"check_perform_sedit('$url','$id');",'escape'=>false,'class'=>'btn btn-warning'));?>
                            <?php echo $this->Html->Link('<span class="glyphicon glyphicon-trash"></span>&nbsp;Delete','#',array('onclick'=>"check_perform_sdelete('$id');",'class'=>'btn btn-danger','escape'=>false));?></td>
                            <td><?php echo $post['Subject']['qbank_count']; ?></td>
                        </tr>
                        <?php endforeach; ?>
                        <?php unset($post); ?>
                        </table>
                </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end();?>
<?php echo $this->element('pagination');?>
<div class="modal fade" id="targetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-content">        
  </div>
</div>