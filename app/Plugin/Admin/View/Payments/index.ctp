<?php echo $this->Session->flash();?>
<div class="row">
    <div class="col-md-12">    
        <div class="panel panel-default">
        <div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">Paypal <span>Payment Options</span></h4>
			</div>
		</div>            
                <div class="panel-body">
                <?php echo $this->Html->link('<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Back To Configuration',array('controller' => 'Configurations','action'=>'index'),array('escape' => false,'class'=>'btn btn-info'));?>
                <?php echo $this->Form->create('Payment', array( 'controller' => 'Payments', 'action' => 'index','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal'));?>
                    <div class="form-group">
                        <label for="site_name" class="col-sm-3 control-label">User Name</label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('username',array('label' => false,'class'=>'form-control input-sm','data-errormessage'=>'User Name Required','placeholder'=>'User Name','div'=>false));?>
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="site_name" class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('password',array('label' => false,'class'=>'form-control input-sm','data-errormessage'=>'Password Required','placeholder'=>'Password','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="site_name" class="col-sm-3 control-label">Signature</label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('signature',array('label' => false,'class'=>'form-control input-sm','data-errormessage'=>'Signature Required','placeholder'=>'Signature','div'=>false));?>
                        </div>
                    </div>                                             
                    <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-refresh"></span>&nbsp;Save Settings</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>