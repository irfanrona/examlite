    <div class="col-md-13">    
        <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <div class="widget">
                <h4 class="widget-title">Forgot <span>Password</span></h4>
            </div>
        </div>        
                <div class="panel-body">
            <?php echo $this->Session->flash();?>
                <?php echo $this->Form->create('Forgot', array( 'controller' => 'Forgots', 'action' => 'password','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal','role'=>'form'));?>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Email :</label>
                    <div class="col-sm-9">
                    <?php echo $this->Form->input('email',array('label' => false,'class'=>'form-control validate[required,custom[email]]','placeholder'=>'Email','div'=>false));?>
                    </div>
                </div>
                <div class="form-group text-center">
                    <div class="col-sm-offset-3 col-sm-2">
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-log-in"></span> Submit</button>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="row">
                <p class="help-block"><?php echo$this->Html->link('Forgot User Name',array('controller'=>'Forgots','action'=>'username'));?></p>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="row">
                <p class="help-block"><?php echo$this->Html->link('Home',array('controller'=>'Users','action'=>'login_form'));?></p>
                    </div>
                </div>
                <?php echo$this->Form->end();?>
            </div>
        </div>
    </div>
