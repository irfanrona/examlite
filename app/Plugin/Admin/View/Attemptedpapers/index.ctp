<div class="row">
    <?php echo $this->element('pagination',array('IsSearch'=>'No','IsDropdown'=>'No'));
        $page_params = $this->Paginator->params();
        $limit = $page_params['limit'];
        $page = $page_params['page'];
        $serial_no = 1*$limit*($page-1)+1;?>
</div>
	<?php echo $this->Session->flash();?>
	<div class="row">
		<div class="col-md-12">
		<div class="btn-group">
		<?php echo $this->Html->link('<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Back To Exam',array('controller' => 'Exams','action'=>'index'),array('escape' => false,'class'=>'btn btn-info'));?>
		</div>
		<?php foreach($Attemptedpapers as $post):?>
			<div class="panel panel-default">
			    <div class="panel-heading">
				<div class="widget">
				<h4 class="widget-title">Attempted Papers of <span><?php echo h($post['Exam']['name']);?></span></h4>
			    </div>
			</div>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">                            
						<tr>
							<td>Name</td>
							<td><?php echo h($post['Student']['name']);?></td>
							<td>Email</td>
							<td><?php echo h($post['Student']['email']);?></td>
						</tr>
						<tr>
							<td>Total Marks</td>
							<td><?php echo$post['Attemptedpaper']['total_marks'];?></td>
							<td>Obtained Marks</td>
							<td><?php echo$post['Attemptedpaper']['obtained_marks'];echo ($post['Attemptedpaper']['user_id']==0)?" (Pending)":"";?></td>
						</tr>
						<tr>
							<td>Result Finalized</td>
							<td><?php $examResultId=$post['Attemptedpaper']['id']; if($post['Attemptedpaper']['user_id']==0){echo$this->Form->postLink('Finalize It',array('action'=>'finalize',$examId,$page,$examResultId),array('confirm'=>'Are you sure, you want to finalize this paper'));}
							else{?><strong class="text-success">Finalized</strong><?php }?></td>
							<td>Finalized By</td>
							<td><?php echo h($post['User']['name']);?></td>
						</tr>                            
					</table>
				</div>
				<div class="panel-body">
					<div class="col-md-13">                    
						<div class="panel-group" id="accordion">
							<?php foreach($post['Question'] as $k=>$ques):?>
								<div class="panel panel-default">
									<div class="panel-heading">
									 <a data-toggle="collapse" href="#collapse<?php echo$ques['ExamStat']['ques_no'];?>">
										  <strong>Question No. <?php echo $ques['ExamStat']['ques_no'];?> (<?php echo$Qtype[$ques['qtype_id']-1]['Qtype']['question_type'];?>)</strong>
										</a>
									</div>
									<div id="collapse<?php echo$ques['ExamStat']['ques_no'];?>" class="collapse<?php echo($k==0)?"in":"";?>">
										<div class="table-responsive">                    
											<table class="table table-bordered">
												
												<tr>
													<td colspan="4"><?php echo str_replace("<script","",$ques['question']);?></td>                                
												</tr>
												<?php if($Qtype[$ques['qtype_id']-1]['Qtype']['type']=="M"){?>
												<?php if(strlen($ques['option1'])>0){?>
												<tr class="text-left">
													<td><strong class="text-danger">Option (1)</strong></td>
													<td colspan="3"><?php echo str_replace("<script","",$ques['option1']);?></td>
												</tr>
												<?php }?>
												<?php if(strlen($ques['option2'])>0){?>
												<tr class="text-left">
												  <td><strong class="text-danger">Option (2</strong>)</td>
												  <td colspan="3"><?php echo str_replace("<script","",$ques['option2']);?></td>
												</tr>
												<?php }?>
												<?php if(strlen($ques['option3'])>0){?>												
												<tr class="text-left">
												  <td><strong class="text-danger">Option (3)</strong></td>
												  <td colspan="3"><?php echo str_replace("<script","",$ques['option3']);?></td>												
												</tr>
												<?php }?>
												<?php if(strlen($ques['option4'])>0){?>
												<tr class="text-left">
												  <td><strong class="text-danger">Option (4)</strong></td>
												  <td colspan="3"><?php echo str_replace("<script","",$ques['option4']);?></td>
												</tr>
												<?php }?>
												<?php if(strlen($ques['option5'])>0){?>
												<tr class="text-left">
												  <td><strong class="text-danger">Option (5)</strong></td>
												  <td colspan="3"><?php echo str_replace("<script","",$ques['option5']);?></td>
												</tr>
												<?php }?>
												<?php if(strlen($ques['option6'])>0){?>
												<tr class="text-left">
												  <td><strong class="text-danger">Option (6)</strong></td>
												  <td colspan="3"><?php echo str_replace("<script","",$ques['option6']);?></td>
												</tr>
												<?php }?>												
												<tr>
													<td colspan="2">Actual Answer: <?php echo$ques['answer'];?></td>
													<td colspan="2">User Selected: <?php echo$ques['ExamStat']['option_selected'];?></td>
												</tr>
												<?php }?>
												<?php if($Qtype[$ques['qtype_id']-1]['Qtype']['type']=="T"){?>                            
												<tr>
													<td colspan="2">Actual Answer: <?php echo$ques['true_false'];?></td>
													<td colspan="2">User Selected: <?php echo$ques['ExamStat']['true_false'];?></td>
												</tr>
												<?php }?>
												<?php if($Qtype[$ques['qtype_id']-1]['Qtype']['type']=="F"){?>                            
												<tr>
													<td colspan="2">Actual Answer: <?php echo h($ques['fill_blank']);?></td>
													<td colspan="2">User Answer: <?php echo h($ques['ExamStat']['fill_blank']);?></td>
												</tr>
												<?php }?>
												<?php if($Qtype[$ques['qtype_id']-1]['Qtype']['type']=="S"){?>                            
												<tr>
													<td colspan="4">User Answer</td>
												</tr>
												<tr>
													<td colspan="4"><?php echo str_replace("<script","",$ques['ExamStat']['answer']);?></td>
												</tr>
												<?php }?>
												<tr>
													<td>Attempted at: <?php echo($ques['ExamStat']['answered']==1)?$this->Time->Format('d-M-Y [H:i:s]',$ques['ExamStat']['attempt_time']):"Not Attempted";?></td>
													<td>Marks: <?php echo$ques['ExamStat']['marks'];?></td>
													<td>Marks Obtained: <?php if($Qtype[$ques['qtype_id']-1]['Qtype']['type']=="S" && $post['Attemptedpaper']['user_id']==0){$id=$ques['ExamStat']['exam_id'];$statId=$ques['ExamStat']['id'];
														echo $this->Form->create('Attemptedpaper',array('controller'=>'Attemptedpaper','action' =>"marksupdate/$id/$statId",'name'=>'post_req','id'=>'post_req','inputDefaults'=>array('label'=>false,'div'=>false)));
														echo $this->Form->input('marks_obtained',array('value'=>$ques['ExamStat']['marks_obtained'],'label'=>false,'div'=>false,'autocomplete'=>'off','size'=>4,'maxlength'=>4));
														echo $this->Form->hidden('page',array('value'=>$page));
														?>&nbsp;<?php echo $this->Form->end(array('label'=>'Update','div'=>false,'class'=>'btn btn-default'));                                    
													}else{echo$ques['ExamStat']['marks_obtained'];}?></td>
													<?php $userName="";
													if($Qtype[$ques['qtype_id']-1]['Qtype']['type']=="S")
													{
														foreach($UserArr as $User):
														if($User['User']['id']==$ques['ExamStat']['user_id'])
														{
															$userName=$User['User']['name'];
															break;
														}
														endforeach;unset($User);
													}?>                                
													<td>Checked by: <?php echo($ques['ExamStat']['user_id']==0)?"System":h($userName);?></td>
												</tr>
												<tr>
													<td colspan="4"><hr/></td>
												</tr>
											</table>
										</div>
									</div>	
								</div>	
							<?php endforeach;unset($ques);?> 
						</div>	
					</div>
				</div>
			</div>
			<?php endforeach;unset($post);?>
		</div>
	</div>