<script type="text/javascript">
    $(document).ready(function(){
        $('#post_req').validationEngine();
        });
</script>
<?php if(!$isError){?>
<div class="container"><?}?>
<div class="row">
<?php echo $this->Session->flash();?>    
    <div class="col-md-12">    
        <div class="panel panel-default mrg">
            <div class="panel-heading"><div class="widget-modal"><h4 class="widget-modal-title">Add <span>Slides</span> <?php if(!$isError){?><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></strong><?php }?></h4></div></div>
                <div class="panel-body">
                <?php echo $this->Form->create('Slide', array( 'controller' => 'Slide', 'action' => 'add','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal','type'=>'file'));?>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label"><small>Slide Name</small></label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('slide_name',array('label' => false,'class'=>'form-control input-sm validate[required]','data-errormessage'=>'Slide Name Required','placeholder'=>'Slide Name','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label"><small>Ordering</small></label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('ordering',array('label' => false,'class'=>'form-control input-sm validate[required,custom[number]]','placeholder'=>'Ordering','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label"><small>Upload Slide (1170*300)</small></label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('photo',array('type' => 'file','label' => false,'class'=>'form-control input-sm','placeholder'=>'Upload Photo','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign"></span> Save</button>
                            <?php if(!$isError){?><button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Close</button><?php }?>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if(!$isError){?>
</div><?php }?>