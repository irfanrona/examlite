<script type="text/javascript">
    $(document).ready(function(){
        $('#post_req').validationEngine();
        });
</script>
<div class="container">
<div class="row">
<?php echo $this->Session->flash();?>
    <div class="col-md-12">    
        <div class="panel panel-default mrg">
            <div class="panel-heading"><div class="widget-modal"><h4 class="widget-modal-title">Edit <span>Slides</span> <?php if(!$isError){?><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></strong><?php }?></h4></div></div>
                <div class="panel-body">
					<?php echo $this->Form->create('Slide', array( 'controller' => 'Slide','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal','type'=>'file'));?>
					<?php foreach ($Slide as $k=>$post): $id=$post['Slide']['id'];$form_no=$k+1;?>
						<div class="panel panel-default">
							<div class="panel-heading"><strong><small class="text-danger">Form <?php echo$form_no?></small></strong></div>
							<div class="panel-body">
								<div class="form-group">
									<label for="group_name" class="col-sm-3 control-label">Slide Name</label>
									<div class="col-sm-9">
									   <?php echo $this->Form->input('slide_name',array('name'=>"data[Slide][$id][slide_name]",'value'=>$post['Slide']['slide_name'],'label' => false,'class'=>'form-control input-sm validate[required]','data-errormessage'=>'Slide Name Required','placeholder'=>'Slide Name','div'=>false));?>
									</div>
								</div>
								<div class="form-group">
									<label for="group_name" class="col-sm-3 control-label">Ordering</label>
									<div class="col-sm-9">
									   <?php echo $this->Form->input('ordering',array('name'=>"data[Slide][$id][ordering]",'value'=>$post['Slide']['ordering'],'label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Ordering Name','div'=>false));?>
									</div>
								</div>
								<div class="form-group">
								    <label for="group_name" class="col-sm-3 control-label">Status</label>
								    <div class="col-sm-9">
									<?php echo $this->Form->select('status',array("Active"=>"Active","Suspend"=>"Suspend"),array('name'=>"data[Slide][$id][status]",'value'=>$post['Slide']['status'],'empty'=>null,'label' => false,'class'=>'form-control input-sm validate[required]','div'=>false));?>
								    </div>
								</div>
								<div class="form-group">
									<label for="group_name" class="col-sm-3 control-label">Upload Slide (1170*300)</label>
									<div class="col-sm-9">
									   <?php echo $this->Form->input('photo',array('type' => 'file','name'=>"data[Slide][$id][photo]",'label' => false,'class'=>'form-control input-sm','div'=>false));?>
									</div>
								</div>
								<div class="form-group text-left">
									<div class="col-sm-offset-3 col-sm-10">
										<?php echo $this->Form->input('id', array('name'=>"data[Slide][$id][id]",'value'=>$post['Slide']['id'],'type' => 'hidden'));?>                            
									</div>
								</div>
							</div>	
						</div>						
                    <?php endforeach; ?>
                        <?php unset($post); ?>
                        <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">                            
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-refresh"></span> Update</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>