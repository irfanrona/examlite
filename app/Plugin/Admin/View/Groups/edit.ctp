<script type="text/javascript">
    $(document).ready(function(){
        $('#post_req').validationEngine();
        });
</script>
<div class="container">
<div class="row">
<?php echo $this->Session->flash();?>
    <div class="col-md-12">    
        <div class="panel panel-default mrg">
            <div class="panel-heading"><div class="widget-modal"><h4 class="widget-modal-title">Edit <span>Groups</span><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></strong></h4></div></div>
                <div class="panel-body">
					<?php echo $this->Form->create('Group', array( 'controller' => 'Group','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal'));?>
					<?php foreach ($Group as $k=>$post): $id=$post['Group']['id'];$form_no=$k+1;?>
						<div class="panel panel-default">
							<div class="panel-heading"><strong><small class="text-danger">Form <?php echo$form_no?></small></strong></div>
							<div class="panel-body">
								<div class="form-group">
									<label for="group_name" class="col-sm-3 control-label">Group Name</label>
									<div class="col-sm-9">
									   <?php echo $this->Form->input('group_name',array('name'=>"data[Group][$id][group_name]",'value'=>$post['Group']['group_name'],'label' => false,'class'=>'form-control input-sm validate[required]','data-errormessage'=>'Group Required','placeholder'=>'Group Name','div'=>false));?>
									</div>
								</div>
								<div class="form-group text-left">
									<div class="col-sm-offset-3 col-sm-10">
										<?php echo $this->Form->input('id', array('name'=>"data[Group][$id][id]",'value'=>$post['Group']['id'],'type' => 'hidden'));?>                            
									</div>
								</div>
							</div>	
						</div>						
                    <?php endforeach; ?>
                        <?php unset($post); ?>
                        <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">                            
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-refresh"></span> Update</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>