<script type="text/javascript">
    $(document).ready(function(){
        $('#post_req').validationEngine();
        });
</script>
<div class="container">
<div class="row">
<?php echo $this->Session->flash();?>
    <div class="col-md-12">    
        <div class="panel panel-default mrg">
            <div class="panel-heading"><div class="widget-modal"><h4 class="widget-modal-title">Add <span>Groups</span><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></strong></h4></div></div>
                <div class="panel-body">
                <?php echo $this->Form->create('Group', array( 'controller' => 'Group', 'action' => 'add','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal'));?>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label"><small>Group Name</small></label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('group_name',array('label' => false,'class'=>'form-control input-sm validate[required]','data-errormessage'=>'Group Required','placeholder'=>'Group Name','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign"></span> Save</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Close</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>