<div class="container">
<div class="row">
<?php echo $this->Session->flash();
?>
    <div class="col-md-12">    
        <div class="panel panel-default mrg">
            <div class="panel-heading"><div class="widget-modal"><h4 class="widget-modal-title">Exam <span>Details</span><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></h4></div></div>
	    <div class="panel-body">
                    <div class="table-responsive"> 
			<table class="table">
			    <tr>
				<td>
				    <div class="chart">
				    <div id="piewrapperqc"></div>
				    <?php echo $this->HighCharts->render("Pie Chartqc");?>
				    </div>
				</td>
			    </tr>
			</table>
		    </div>
		    <div class="table-responsive"> 
			<table class="table">			
			    <tr>
				<td>
				    <div class="chart">
				    <div id="mywrapperdl"></div>
				    <?php echo $this->HighCharts->render("My Chartdl");?>
				    </div>
				</td>
			    </tr>
			</table>
		    </div>
                </div>
            </div>
        </div>
    </div>
</div>
