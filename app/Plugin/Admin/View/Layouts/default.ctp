<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="google-translate-customization" content="839d71f7ff6044d0-328a2dc5159d6aa2-gd17de6447c9ba810-f"></meta>
	<?php echo $this->Html->charset();?>
	<title>
		<?php echo $siteTitle;?>
	</title>
	<meta name="description" content="<?php echo$siteDescription;?>"/>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('/Admin/css/bootstrap.min');
                echo $this->Html->css('/Admin/css/style');
		echo $this->Html->css('design200/style');		
		echo $this->Html->css('design200/fonts');
		echo $this->Html->css('design200/plugins');
		echo $this->Html->css('design200/responsive');
		echo $this->Html->css('design200/settings');
                echo $this->Html->css('/Admin/css/validationEngine.jquery');
		echo $this->fetch('meta');		
		echo $this->fetch('css');
                echo $this->Html->script('/Admin/js/jquery-1.8.2.min');
		echo $this->Html->script('/Admin/js/html5shiv');
                echo $this->Html->script('/Admin/js/respond.min');                
                echo $this->Html->script('/Admin/js/bootstrap.min');
                echo $this->Html->script('/Admin/js/jquery.validationEngine-en');
                echo $this->Html->script('/Admin/js/jquery.validationEngine');
		echo $this->Html->script('design200/superfish');
		echo $this->Html->script('design200/sidebar');
		echo $this->Html->script('design200/functions');
		echo $this->Html->script('design200/plugins.min');
		echo $this->Html->script('design200/revolution.min');
		echo $this->Html->script('/Admin/js/custom.min');
		echo $this->fetch('script');
                echo $this->Js->writeBuffer();
		$UserArr=$this->Session->read('User');
?>
<?php if($translate>0){?>
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<?php }?>
</head>
<body>
    <?php if($this->Session->check('User')){?>
     <div id="wrap">
	    <div class="main-inner-content">
	    <header id="header" class="section cover header-bg" data-bg="<?php echo $this->webroot.'/img/'; ?>/design200/slider.png">	   
	    <div class="logo-header">
	    <div class="container">
	    <div class="ulogo-container clearfix">
	    <div class="logo pull-left">
	    <h1 class="site-title">
	    Welcome to <?php echo$siteName;?> Admin Dashboard
	    </h1> 
	    <p class="site-desc"><?php echo$siteName;?></p>
	    </div> 
	    <div class="social-info pull-right hidden-xs hidden-sm">
			<div class="col-md-4 mrg text-center">
				<div class="btn-group user-login">
					<div class="btn dropdown-toggle" style="background: none;border: 0px;box-shadow: none;" data-toggle="dropdown">
						<span class="glyphicon glyphicon-user btn btn-success btn-sm"></span>&nbsp;Welcome ! <?php echo $UserArr['User']['name'];?>&nbsp;<span class="caret"></span>
					</div>
					<div class="dropdown-menu" role="menu">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table" style="margin-bottom:0px">
									<tbody>
										<tr>
										  <td  colspan="2"><?php echo $this->Html->link('<span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;My Profile',array('controller' => 'Users','action' => 'myProfile'),array('escape' => false,'class'=>'btn btn-primary btn-xs btn-block'));?></td>
										</tr>											
										<tr>
										  <td><?php echo $this->Html->link('<span class="glyphicon glyphicon-cog"></span>&nbsp;Change Password',array('controller' => 'Users','action' => 'changePass'),array('escape' => false,'class'=>'btn btn-primary btn-xs'));?>
											<td><?php echo $this->Html->link('<span class="glyphicon glyphicon-off"></span>&nbsp;Signout',array('controller' => 'Users', 'action' => 'logout'),array('escape' => false,'class'=>'btn btn-danger btn-xs'));?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>	
				</div>
				<strong><small><div id="clock"></div></small></strong>
			</div>
		</div>
	</div> 
	    </div> 
	    </div> 
	    <div class="site-menu">
	    <div class="container">
	    <div class="site-menu-inner clearfix">
	    <div class="site-menu-container pull-left">
	    <nav class="hidden-xs hidden-sm">
	    <ul class="sf-menu clearfix list-unstyled">
	      <?php foreach ($menuArr as $menu):
	      $menuIcon=$menu['Page']['icon'];$menuName=$menu['Page']['model_name'];?>
	      <li <?php echo (strtolower($this->params['controller'])==strtolower($menu['Page']['controller_name']) || in_array(strtolower($this->params['controller']),explode(",",strtolower($menu['Page']['sel_name']))))?"class=\"current-menu-item\"":"";?>><?php echo $this->Html->link("<span class=\"glyphicon $menuIcon\"></span>&nbsp;$menuName",array('controller' => $menu['Page']['controller_name'],'action'=>$menu['Page']['action_name']),array('escape' => false));?></li>
	      <?php endforeach;unset($menu);?>
	      <li class="menu-hidden"><?php echo $this->Html->link('<span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;My Profile',array('controller' => 'Users','action' => 'myProfile'),array('escape' => false));?></li>
	      <li class="menu-hidden"><?php echo $this->Html->link('<span class="glyphicon glyphicon-cog"></span>&nbsp;Change Password',array('controller' => 'Users','action' => 'changePass'),array('escape' => false));?></li>
	      <li class="menu-hidden"><?php echo $this->Html->link('<span class="glyphicon glyphicon-off"></span>&nbsp;Signout',array('controller' => 'Users', 'action' => 'logout'),array('escape' => false));?></li>
	    </ul> 
	    </nav> 
	    </div> 	    
	    </div> 
	    </div> 
	    </div>
	    </header>
        <?php } else{?>
	 <div id="wrap">
	    <div class="main-inner-content">
	    <header id="header" class="section cover header-bg" data-bg="<?php echo $this->webroot.'/img/'; ?>/design200/slider.png">	    
	<div class="container">
		<div class="col-md-12 mrg-1 text-center"><h3><strong><i><p align="center"><?php echo$this->Html->image('logo-website.fw.png',array('alt'=>'Edu Expression','width'=>'300','align'=>'center','class'=>'img-responsive'));?> </p><p>Exam Control Panel</p></i></strong></h3></div>
        </div>
	</header>
        <?php }?>
		<div id="page-content">
	<div class="container mrg">
                    <?php echo $this->fetch('content'); ?>
		</div>
	</div>
	    </div>
               <footer id="footer">
<div class="footer-widget">
<div class="container">
<div class="row">
<div class="col-md-6">
<div class="widget">
<h5 class="widget-title">
Copyright &copy; <?php echo $this->Time->format('Y',$siteYear);?><span> <?php echo$siteName;?></span>
</h4>
</div> 
</div> 
<div class="col-md-6 text-right">
<div class="widget">
<h5 class="widget-title">
Time <span><?php echo $this->Time->format('d-m-Y h:i:s A',$siteTimezone);?></span>
</h4>
</div> 
</div> 
</div> 
</div> 
</div>
<div class="footer-credits">
<div class="container">
<div class="footer-credits-inner">
<div class="row">
<div class="col-md-6">
<span>Powered by <?php echo$this->Html->Link('Mahna.id','https://www.mahna.id',array('target'=>'_blank'));?></span>
</div> 
</div> 
</div> 
</div> 
</div> 
</footer>
	<div class="sb-slidebar sb-right">
<div class="sb-menu-trigger"></div>
	</div>
   </div>
</body>
</html>