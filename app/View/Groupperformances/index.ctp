<?php echo $this->Session->flash();?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
        <div class="panel-heading">
            <div class="widget">
                <h4 class="widget-title">Your performace  <span>compared to your group</span></h4>
            </div>
        </div>        
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <td>
                            <div class="chart">
                            <div id="mywrapperdl"></div>
                            <?php echo $this->HighCharts->render("My Chartdl");?>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>