<?php echo $this->Session->flash();?>
<div class="row">
    <div class="col-md-12">    
        <div class="panel panel-default">
            <div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">Payment From <span>Pay Pal</span></h4>
			</div>
		</div>            
                <div class="panel-body">
                <?php echo $this->Form->create('Payment', array( 'controller' => 'Payment', 'action' => 'checkout','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal'));?>
                     <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label">Amount</label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('amount',array('label' => false,'class'=>'form-control input-sm validate[required,custom[number],min[1]]','placeholder'=>'Amount','div'=>false));?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="group_name" class="col-sm-3 control-label">Remarks</label>
                        <div class="col-sm-9">
                           <?php echo $this->Form->input('remarks',array('label' => false,'class'=>'form-control input-sm validate[required]','placeholder'=>'Remarks','div'=>false));?>
                        </div>
                    </div>                    
                    <div class="form-group text-left">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-shopping-cart"></span> Pay From Pay Pal</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
    </div>