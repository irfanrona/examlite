<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">
		<div class="widget">
		    <h4 class="widget-title">List of <span>Exams</span></h4>
		</div>
	    </div>
            <ul>
            <?php foreach($examList as $examValue):?>
            <li><?php echo$this->Html->link($examValue['Exam']['name'],array('action'=>'view',$examValue['Result']['id']));?></li>
            <?php endforeach;unset($examValue);?>
            <li><?php echo$this->Html->link('All',array('action'=>'index'));?></li>
            </ul>
        </div>
    </div>    
    <div class="col-md-9">
    <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
		<div class="widget">
		    <h5 class="widget-title">Graphical report of <span><?php echo h($examDetails['Exam']['name']);?></span></h5>
		</div>
	    </div>		
                        <div class="chart">
                            <div id="mywrapperdl"></div>
                            <?php echo $this->HighCharts->render("My Chartdl");?>
                        </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
		<div class="widget">
		    <h4 class="widget-title"><span>Details</span></h4>
		</div>
	    </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <td><strong class="text-danger">Name</strong></td>
                                    <td><?php echo h($examDetails['Exam']['name']);?></td>
                                </tr>
                                <tr>
                                    <td><strong class="text-danger">Finalizez by</strong></td>
                                    <td><?php echo h($examDetails['User']['name']);?></td>
                                </tr>
                                 <tr>
                                    <td><strong class="text-danger">Pass Criteria</strong></td>
                                    <td><?php echo$this->Number->toPercentage($examDetails['Exam']['passing_percent']);?></td>
                                </tr>
                                <tr>
                                    <td><strong class="text-danger">Obtained</strong></td>
                                    <td><?php echo$this->Number->toPercentage($examDetails['Result']['percent']);?></td>
                                </tr>
                                <tr>
                                    <td><strong class="text-danger">Result</strong></td>
                                    <td><?php echo h($examDetails['Result']['result']);?></td>
                                </tr>                                
                                <tr>
                                    <td><strong class="text-danger">Attempted</strong></td>
                                    <td><?php echo$this->Time->format('d M, Y [H:i]',$examDetails['Result']['start_time']);?></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
			</div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
		<div class="widget">
		    <h4 class="widget-title">Detailed <span>Marks Sheet</span></h4>
		</div>
	    </div>		
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th>Subject</th>
                                    <th>Total Marks</th>
                                    <th>Obtained</th>
                                    <th>Percentage</th>
                                </tr>
                                <?php foreach($userMarksheet as $userValue):?>
                                <tr>                                    
                                    <td class="text-danger"><strong><?php echo h($userValue['Subject']['name']);?></strong></td>
                                    <td><?php echo$userValue['Subject']['total_marks'];?></td>
                                    <td><?php echo$userValue['Subject']['obtained_marks'];?></td>
                                    <td><?php echo CakeNumber::toPercentage($userValue['Subject']['percent']);?></td>
                                </tr>
                                <?php endforeach;unset($userValue);?>
                                <?php if($examDetails['Exam']['declare_result']=="Yes"){?>
                                <tr>
                                    <td colspan="4"><?php echo $this->Html->link('<span class="glyphicon glyphicon-briefcase"></span>&nbsp;Attempted Papers',array('action'=>'attemptedpapers',$id),array('escape' => false,'class'=>'btn btn-info'));?>
                                </tr>
                                <?php }?>
                            </table>
                        </div>                            
                    </div>
                </div>
            </div>
    </div>
    </div>
</div>