<?php echo $this->Session->flash();?>
<div class="row">
	<?php echo $this->element('pagination',array('IsSearch'=>'No','IsDropdown'=>'No'));
	$page_params = $this->Paginator->params();
	$limit = $page_params['limit'];
	$page = $page_params['page'];
	$serial_no = 1*$limit*($page-1)+1;?>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">My <span>Results</span></h4>
			</div>
		</div>		
			<div class="table-responsive">
				<table class="table table-striped">
				<tr>
					<th>S.No.</th>
					<th>Exam Name</th>
					<th>Attempt Date</th>
					<th>Result</th>
					<th>Marks</th>
					<th>Percentage</th>
					<th>View Details</th>
				</tr>
				<?php foreach($Result as $post):?>
				<tr>
					<td><?php echo$serial_no++;?></td>
					<td><?php echo h($post['Exam']['name']);?></td>
					<td><?php echo$this->Time->format('d M, Y [H:i]',$post['Result']['start_time']);?></td>
					<td><?php echo$post['Result']['result'];?></td>
					<td><?php echo$post['Result']['obtained_marks'];?>&nbsp;/&nbsp;<?php echo$post['Result']['total_marks'];?></td>
					<td><?php echo$this->Number->toPercentage($post['Result']['percent']);?></td>
					<td><?php echo$this->Html->link('View',array('action'=>'view',$post['Result']['id']));?></td>
				</tr>
				<?php endforeach;unset($post);?>
				</table>
			</div>
		</div>
	</div>
</div>