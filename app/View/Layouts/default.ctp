<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="google-translate-customization" content="839d71f7ff6044d0-328a2dc5159d6aa2-gd17de6447c9ba810-f"></meta>
	<?php echo $this->Html->charset();?>
	<title>
		<?php echo $siteTitle;?>
	</title>
	<meta name="description" content="<?php echo$siteDescription;?>"/>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('bootstrap.min');
                echo $this->Html->css('style');
		echo $this->Html->css('design200/style');		
		echo $this->Html->css('design200/fonts');
		echo $this->Html->css('design200/plugins');
		echo $this->Html->css('design200/responsive');
		echo $this->Html->css('design200/settings');
                echo $this->Html->css('validationEngine.jquery');
		if(strtolower($this->params['controller'])=="exams" && strtolower($this->params['action'])=="start")
		echo $this->Html->css('jquery.countdown');
		echo $this->fetch('meta');		
		echo $this->fetch('css');
                echo $this->Html->script('jquery-1.8.2.min');
		echo $this->Html->script('html5shiv');
                echo $this->Html->script('respond.min');                
                echo $this->Html->script('bootstrap.min');
                echo $this->Html->script('jquery.validationEngine-en');
                echo $this->Html->script('jquery.validationEngine');
		echo $this->Html->script('design200/superfish');
		echo $this->Html->script('design200/sidebar');
		echo $this->Html->script('design200/functions');
		echo $this->Html->script('design200/plugins.min');
		echo $this->Html->script('design200/revolution.min');
		echo $this->Html->script('custom.min');
		if(strtolower($this->params['controller'])=="exams" && strtolower($this->params['action'])=="start")
		echo $this->Html->script('jquery.countdown.min');
		echo $this->fetch('script');
                echo $this->Js->writeBuffer();
		$UserArr=$this->Session->read('Student');
?>
	<?php if($translate>0){?>
	  <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
	  <?php }?>
</head>
<body>
	<?php if($this->Session->check('Student')){?>
   		<div id="wrap">
			<div class="main-inner-content">
				<header id="header" class="section cover header-bg" data-bg="<?php echo $this->webroot.'/img/'; ?>/design200/slider.png">
				<?php if(strtolower($this->params['controller'])!="exams" && strtolower($this->params['action'])!="start"){?>
				<div class="header-fixed">
				<div class="logo-header">
				<div class="container">
				<div class="logo-container pull-left clearfix">
				<div class="logo">
				<h1 class="site-title">
				Welcome to <?php echo$siteName;?>&nbsp;Dashboard
				</h1> 
				<p class="site-desc"><?php echo$siteName;?></p>
			</div> 
	    </div> 
	    <div class="menu-container pull-right">
	    <div class="site-menu">
	    <div class="site-menu-inner clearfix">
	    <div class="site-menu-container pull-left">
	    <nav class="hidden-xs hidden-sm">
	    <ul class="sf-menu clearfix list-unstyled">
	      <?php foreach($menuArr as $menuName=>$menu): $menuIcon=$menu['icon'];?>
	      <li <?php echo (strtolower($this->params['controller'])==strtolower($menu['controller']))?"class=\"current-menu-item\"":"";?>><?php echo $this->Html->link("<span class=\"$menuIcon\"></span>&nbsp;$menuName",array('controller' => $menu['controller'],'action'=>$menu['action']),array('escape' => false));?></li>
	      <?php endforeach;unset($menu);unset($menuName);unset($menuIcon);?>
	      <li class="menu-hidden"><?php echo $this->Html->link('<span class="glyphicon glyphicon-user"></span>&nbsp;My Profile',array('controller' => 'Profiles','action' => 'index'),array('escape' => false));?></li>
	      <li class="menu-hidden"><?php echo $this->Html->link('<span class="glyphicon glyphicon-cog"></span>&nbsp;Change Password',array('controller' => 'Profiles','action' => 'changePass'),array('escape' => false));?></li>
	      <li class="menu-hidden"><?php echo $this->Html->link('<span class="glyphicon glyphicon-off"></span>&nbsp;Signout',array('controller' => 'Users', 'action' => 'logout'),array('escape' => false));?></li>
	    </ul> 
	    </nav> 
	    </div> 
	    
	    </div> 
	    </div> 
	    </div>
	    </div> 
	    </div> 
	    </div>
	    <?php }?>
	    <div class="logo-header">
	    <div class="container">
	    <div class="ulogo-container clearfix">
	    <div class="logo pull-left">
	    <h1 class="site-title">
	    Welcome to <?php echo$siteName;?>&nbsp;Dashboard
	    </h1> 
	    <p class="site-desc"><?php echo$siteName;?></p>
	    </div> 
	    <div class="social-info pull-right hidden-xs hidden-sm">
	    <div class="col-md-4 text-center">
				<div class="btn-group user-login">
					<div class="btn dropdown-toggle" style="background: none;border: 0px;box-shadow: none;" data-toggle="dropdown">
						<span class="glyphicon glyphicon-user btn btn-success btn-sm"></span>&nbsp;Welcome ! <?php echo h($UserArr['Student']['name']);?> <span class="caret"></span>
						<?php if($frontPaidExam>0){?><br/><span><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wallet Balance : <?php echo$walletBalance;?></strong></span><?php }?>
					</div>
					<div class="dropdown-menu" role="menu">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table" style="margin-bottom:0px">
									<tbody>
										<tr>
											<td colspan="2"><?php echo $this->Html->link('<span class="glyphicon glyphicon-user"></span>&nbsp;My Profile',array('controller' => 'Profiles','action' => 'index'),array('escape' => false,'class'=>'btn btn-primary btn-xs btn-block'));?></td>
										</tr>											
										<tr>
										  <td><?php echo $this->Html->link('<span class="glyphicon glyphicon-cog"></span>&nbsp;Change Password',array('controller' => 'Profiles','action' => 'changePass'),array('escape' => false,'class'=>'btn btn-primary btn-xs'));?>
											<td><?php echo $this->Html->link('<span class="glyphicon glyphicon-off"></span>&nbsp;Signout',array('controller' => 'Users', 'action' => 'logout'),array('escape' => false,'class'=>'btn btn-danger btn-xs'));?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>	
				</div>
				<strong><small><div id="clock"></div></small></strong>
			</div>
	    </div> 
	    </div> 
	    </div> 
	    </div> 
	    <div class="site-menu">
	    <div class="container">
	    <div class="site-menu-inner clearfix">
	    <div class="site-menu-container pull-left">
	    <nav class="hidden-xs hidden-sm">
	    <ul class="sf-menu clearfix list-unstyled">
	      <?php foreach($menuArr as $menuName=>$menu): $menuIcon=$menu['icon'];?>
	      <li <?php echo (strtolower($this->params['controller'])==strtolower($menu['controller']))?"class=\"current-menu-item\"":"";?>><?php echo $this->Html->link("<span class=\"$menuIcon\"></span>&nbsp;$menuName",array('controller' => $menu['controller'],'action'=>$menu['action']),array('escape' => false));?></li>
	      <?php endforeach;unset($menu);unset($menuName);unset($menuIcon);?>
	    </ul> 
	    </nav> 
	    </div> 	    
	    </div> 
	    </div> 
	    </div>
	    </header>
	<div id="page-content">
	<div class="container mrg">
                    <?php echo $this->fetch('content'); ?>
		</div>
	</div>
	    </div>
	<?php } else{?>	
	  <div id="wrap">
	    <div class="main-inner-content">
	    <header id="header" class="section cover header-bg" data-bg="<?php echo $this->webroot.'/img/'; ?>/design200/slider.png">
	    <div class="header-fixed">
	    <div class="logo-header">
	    <div class="container">
	    <div class="logo-container pull-left clearfix">
	    <div class="logo">
	    <h1 class="site-title">
	    <?php if(strlen($frontLogo)>0){?><?php echo$this->Html->image($frontLogo,array('alt'=>$siteName,'class'=>'img-thumbnail'));} else{?>
		    <h1><?php echo$siteName;?></h1>
		    <?php }?>
	    </h1> 
	    <p class="site-desc"><?php echo$siteName;?></p>
	    </div> 
	    </div> 
	    <div class="menu-container pull-right">
	    <div class="site-menu">
	    <div class="site-menu-inner clearfix">
	    <div class="site-menu-container pull-left">
	    <nav class="hidden-xs hidden-sm">
	    <ul class="sf-menu clearfix list-unstyled">
	      <?php foreach($frontmenuArr as $menuName=>$menu): $menuIcon=$menu['icon'];if($this->params['controller']=="pages"){$this->params['controller']="";}$isMenu=true;
	      if($menuName=="Sign Up" && $frontRegistration!=1){$isMenu=false;}?>
	      <li <?php if($isMenu==true){ echo (strtolower($this->params['controller'])==strtolower($menu['controller']))?"class=\"current-menu-item\"":"";?>><?php echo $this->Html->link("<span class=\"$menuIcon\"></span>&nbsp;$menuName",array('controller' => $menu['controller'],'action'=>$menu['action']),array('escape' => false));?></li>
	      <?php } endforeach;unset($menu);unset($menuName);unset($menuIcon);?>
	    </ul> 
	    </nav> 
	    </div> 	   
	    </div> 
	    </div> 
	    </div>
	    </div> 
	    </div> 
	    </div> 
	    <div class="logo-header">
	    <div class="container">
	    <div class="logo-container clearfix">
	    <div class="logo pull-left">
	    <h1 class="site-title">
	    <?php if(strlen($frontLogo)>0){?><?php echo$this->Html->image($frontLogo,array('alt'=>$siteName,'class'=>'img-thumbnail'));} else{?>
		    <h1><?php echo$siteName;?></h1>
		    <?php }?>
	    </h1> 
	    <p class="site-desc"><?php echo$siteName;?></p>
	    </div> 
	    <div class="social-info pull-right hidden-xs hidden-sm">
	    <ul class="social textcolor list-unstyled">
	    <?php if(strlen($contact[0])>0){?>
	    <li class="phone active"><a href="#"><i class="fa fa-phone"></i><span><?php echo$contact[0];?></span></a></li><?php }?>
	    <?php if(strlen($contact[1])>0){?>
	    <li class="mail"><a href="mailto:<?php echo$contact[1];?>"><i class="fa fa-envelope"></i><span><?php echo$contact[1];?></span></a></li><?php }?>
	    <?php if(strlen($contact[2])>0){?>
	    <li class="facebook"><a href="<?php echo$contact[2];?>" target="_blank"><i class="fa fa-facebook"></i><span>follow on facebook</span></a></li><?php }?>
	    </ul> 
	    </div> 
	    </div> 
	    </div> 
	    </div> 
	    <div class="site-menu">
	    <div class="container">
	    <div class="site-menu-inner clearfix">
	    <div class="site-menu-container pull-left">
	    <nav class="hidden-xs hidden-sm">
	    <ul class="sf-menu clearfix list-unstyled">
	      <?php foreach($frontmenuArr as $menuName=>$menu): $menuIcon=$menu['icon'];if($this->params['controller']=="pages"){$this->params['controller']="";}$isMenu=true;
	      if($menuName=="Sign Up" && $frontRegistration!=1){$isMenu=false;}?>
	      <li <?php if($isMenu==true){ echo (strtolower($this->params['controller'])==strtolower($menu['controller']))?"class=\"current-menu-item\"":"";?>><?php echo $this->Html->link("<span class=\"$menuIcon\"></span>&nbsp;$menuName",array('controller' => $menu['controller'],'action'=>$menu['action']),array('escape' => false));?></li>
	      <?php } endforeach;unset($menu);unset($menuName);unset($menuIcon);?>	   
	    </ul> 
	    </nav> 
	    </div> 	    
	    </div> 
	    </div> 
	    </div> 			
			<?php if($frontSlides==1){?>
			<?php echo $this->Html->script('design200/slider');
			      echo $this->fetch('script');?>
			<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
			  <div id="rev_slider_1_1" class="rev_slider fullwidthabanner">
			  <ul>
			  <?php foreach($slides as $k=>$value): $photoImg='slides_thumb/'.$value['Slide']['photo'];?>
			  <li data-transition="random-static" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
			  <?php echo $this->Html->image('design200/transparent.png',array('alt'=>'','data-bgposition'=>'center top','data-bgfit'=>'cover','data-bgrepeat'=>'no-repeat'));?>
			  <div class="tp-caption sfb stl" data-x="40" data-y="40" data-speed="600" data-start="500" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="600" style="z-index: 2;">
			  <?php echo $this->Html->image($photoImg,array('alt'=>$value['Slide']['slide_name'],'width'=>'1140'));?>
			  </div>
			  </li>
			  <?php endforeach;unset($k);unset($value);?>
			  </ul>
			  <div class="tp-bannertimer tp-bottom hidden"></div>
			  </div> 
			  </div> 
			<?php }?>	    
	    </header>
	<div id="page-content">	
	<div class="container mrg">
		<div class="row">
			<div class="col-md-3">
				<div id="sidebar">
				  <div class="sidebar-inner">
				  <div class="widget category">
				    <h4 class="widget-title">
				    Latest <span>News</span>
				    </h4>
				    <ul>				
					<?php foreach($news as $value):$id=$value['News']['id'];?>
					<li><?php echo$this->Html->link($value['News']['news_title'],array('controller'=>'News','action'=>'show',$id));?></li>
					<?php endforeach;unset($value);?>				  
				  </ul>
				    </div>
				  </div>
				</div>
			</div>
			 <?php echo $this->fetch('content'); ?>		
		</div>	
	</div>
	</div> 
</div>
	  </div>
	<?php }?>
	<footer id="footer">
<div class="footer-widget">
<div class="container">
<div class="row">
<div class="col-md-6">
<div class="widget">
<h5 class="widget-title">
Copyright &copy; <?php echo $this->Time->format('Y',$siteYear);?><span> <?php echo$siteName;?></span>
</h4>
</div> 
</div> 
<div class="col-md-6 text-right">
<div class="widget">
<h5 class="widget-title">
Time <span><?php echo $this->Time->format('d-m-Y h:i:s A',$siteTimezone);?></span>
</h4>
</div> 
</div> 
</div> 
</div> 
</div>
<div class="footer-credits">
<div class="container">
<div class="footer-credits-inner">
<div class="row">
<div class="col-md-6">
<span>Powered by <?php echo$this->Html->Link('Mahna.id','https://www.mahna.id',array('target'=>'_blank'));?></span>
</div> 
</div> 
</div> 
</div> 
</div> 
</footer>
	<div class="sb-slidebar sb-right">
<div class="sb-menu-trigger"></div>
	</div>
   </div>
</body>
</html>