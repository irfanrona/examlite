<div class="col-md-9">
				<div class="page-heading">
				  <div class="widget">
				  <h2 class="widget-title">Student <span>Login</span></h2>
				  </div>
				</div>
					<?php echo $this->Session->flash();?>
						<?php echo $this->Form->create('User', array( 'controller' => 'Users', 'action' => 'login','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal','role'=>'form'));?>
						  <div class="form-group">
							<label for="inputEmail3" class="col-sm-3 control-label">Email :</label>
							<div class="col-sm-9">
							 <?php echo $this->Form->input('email',array('label' => false,'class'=>'form-control validate[required,custom[email]]','placeholder'=>'Email','div'=>false));?>
							</div>
						  </div>
						  <div class="form-group">
							<label for="inputPassword3" class="col-sm-3 control-label">Password</label>
							<div class="col-sm-9">
							  <?php echo $this->Form->input('password',array('label' => false,'class'=>'form-control input-sm validate[required,minSize[4],maxSize[15]]','placeholder'=>'Password','div'=>false));?>
							</div>
						  </div>
						 <div class="form-group text-center">
								<div class="col-sm-offset-3 col-sm-2">
								<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-log-in"></span> Sign In</button>
								</div>
							    </div>
							<div class="form-group">								
								<div class="col-md-12">
									<div class="col-md-3">
										<?php echo$this->Html->link('<span class="glyphicon glyphicon-cog"></span>&nbsp; Forgot Password',array('controller'=>'forgots','action'=>'password'),array('escape'=>false));?>
									</div>
									<?php if($frontRegistration==1){?>
									<div class="col-md-4">
										<?php echo$this->Html->link('<span class="glyphicon glyphicon-user"></span>&nbsp; New User? Create Account',array('controller'=>'Registers','action'=>'index'),array('escape'=>false));?>
									</div>
									<div class="col-md-4">
										<?php echo$this->Html->link('<span class="glyphicon glyphicon-share-alt"></span>&nbsp; Re-Send Email Verification',array('controller'=>'Emailverifications','action'=>'resend'),array('escape'=>false));?>
									</div>
									<?php }?>
								</div>								
							</div>
						<?php echo$this->Form->end();?>				
			</div>