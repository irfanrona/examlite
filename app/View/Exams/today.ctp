<?php echo $this->Session->flash();?>
<div class="row">
	<div class="col-md-2">
		<div class="panel panel-default">
		<div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">My <span>Exam</span></h4>
			</div>
		</div>
		<div class="list-group">
			<?php echo$this->Html->link("Today's Exam",array('controller'=>'Exams','action'=>'Today'),array('class'=>'list-group-item active'));?>
			<?php echo$this->Html->link('Upcoming Exam',array('controller'=>'Exams','action'=>'Upcoming'),array('class'=>'list-group-item'));?>			
		</div>
		</div>
	</div>
<div class="col-md-10">
		<div class="panel panel-default">
		<div class="panel-heading">
			<div class="widget">
				<h4 class="widget-title">Today's <span>Exam</span></h4>
			</div>
		</div>
			<div class="table-responsive">
				<table class="table table-striped">
					<?php if($todayExam){?>
					<tr>
						<th colspan="6">These are the exam(s) that can be taken right now.</th>
					</tr>
					<tr>						
					</tr>
					<tr>
					<?php foreach($todayExam as $post): $id=$post['Exam']['id'];
					$viewUrl=$this->Html->url(array('controller'=>'Exams','action'=>"view/$id"));
					$instructionUrl=$this->Html->url(array('controller'=>'Exams','action'=>"instruction/$id"));?>
					<tr>
						<td><?php echo h($post['Exam']['name']);?></td>
						<td><?php echo$post['Exam']['total_marks'];?> Marks</td>
						<td><?php echo$this->Time->format('d-m-Y',$post['Exam']['start_date']);?></td>
						<?php if($post['Exam']['paid_exam']=="1"){?><td> Amount <?php echo$post['Exam']['amount'];?></td><?php }else{?><td>&nbsp;</td><?php }?>
						<td><?php echo$this->Html->link('View Details','#',array('onclick'=>"show_modal('$viewUrl');"));?></td>
						<td><?php echo$this->Html->link('Attempt Now','#',array('onclick'=>"show_modal('$instructionUrl');"));?></td>
					</tr>
					<?php endforeach;
					unset($post);unset($id);}else{?>
					<tr>
						<th colspan="6">No Exams found for today.</th>
					</tr>
					<?php }?>
				</table>
			</div>
		</div> 
	</div>	
</div>
<div class="modal fade" id="targetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-content">        
  </div>
</div>