<div class="container">
	<div class="row">
		<div class="col-md-12 mrg">
			<div class="panel panel-default">
			<div class="panel-heading">
				<div class="widget-modal">
					<h4 class="widget-modal-title"><span>Instructions</span>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</h4>
				</div>
			</div>
			<div class="panel-body">
				<?php echo str_replace("<script","",$post['Exam']['instruction']);?>
				<?php
				if($post['Exam']['paid_exam']==1)
				{
					if($ispaid==true)
					{?>
					<p><?php echo $this->Form->postLink('Exam Start',array('action' => 'start', $post['Exam']['id']),array('confirm' => 'After starting the exam timer will not stop. Do you want to start?','class'=>'btn btn-success'));?></p>
					<?php }
					else{?>
					<p><?php echo $this->Form->postLink('Exam Start',array('action' => 'paid', $post['Exam']['id']),array('confirm' => 'This Exam is paid. Amount should be deducted on your wallet automatically. After starting the exam timer will not stop. Do you want to pay & start?','class'=>'btn btn-danger'));?></p>
					<?php }
				}
				else
				{?>
					<p><?php echo $this->Form->postLink('Exam Start',array('action' => 'start', $post['Exam']['id']),array('confirm' => 'After starting the exam timer will not stop. Do you want to start?','class'=>'btn btn-success'));?></p>
				<?php }?>
			</div>
		</div>
	</div>
</div>