<?php echo $this->Session->flash();?>
			<div class="panel panel-default">
					<div class="panel-heading">
		<div class="widget">
                    <h4 class="widget-title">View <span>Student Information</span></h4>
                </div>
            </div>
					<div class="panel-body">
						<div class="col-md-2 text-center">
							<p><div class="img-thumbnail"><?php echo $this->Html->image($std_img, array('alt' => h($post['Student']['name'])));?></div></p>
							<?php echo$this->Html->link('Update Photo',array('action'=>'changePhoto'),array('class'=>'btn btn-success btn-sm btn-block'));?>
							<?php echo$this->Html->link('Change Password',array('controller'=>'Profiles','action'=>'changePass'),array('class'=>'btn btn-danger btn-sm btn-block'));?>
						</div>
						<div class="col-md-10"> 
							<div class="row">
								<table class="table table-striped table-bordered">
									<tr class="text-danger">
										<td><strong><small>Full Name</small></strong></td>
										<td><strong><small>Phone Number</small></strong></td>
									</tr>
									<tr>
										<td><strong><small><?php echo h($post['Student']['name']);?></small></strong></td>
										<td><strong><small><?php echo h($post['Student']['phone']);?></small></strong></td>
									</tr>
									<tr class="text-danger">
										<td><strong><small>Registered Email</small></strong></td>
										<td><strong><small>Alternate Number</small></strong></td>
									</tr>
									<tr>
										<td><strong><small><?php echo h($post['Student']['email']);?></small></strong></td>
										<td><strong><small><?php echo h($post['Student']['guardian_phone']);?></small></strong></td>
									</tr>
									</tr>
									<tr class="text-danger">
										<td><strong><small>Enrolment No.<strong><small></td>
										<td><strong><small>Admission Date</small></strong></td>
									</tr>
									<tr>
										<td><strong><small><?php echo h($post['Student']['enroll']);?></small></strong></td>
										<td><strong><small><?php echo $this->Time->format('d-m-Y',$post['Student']['created']); ?></small></strong></td>
									</tr>
									<tr class="text-danger"> 
										<td><strong><small>Group/s <strong><small></td>
										<td></td>
									</tr>
									<tr>
										<td><small><strong>
										<?php foreach($groupSelect as $groupValue):
										echo h($groupValue['Groups']['group_name']);?> |
										<?php endforeach;unset($groupValue);?>
										</small></strong></td>
										<td><?php echo$this->Html->link('Edit Profile',array('action'=>'editProfile'),array('class'=>'btn btn-success btn-sm'));?></td>
									</tr>
								</table>
							</div>
						</div>
					</div>	
				</div>
      