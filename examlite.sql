/*
 Navicat Premium Data Transfer

 Source Server         : LOKAL Mahna
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : examlite

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 02/10/2019 15:18:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for configurations
-- ----------------------------
DROP TABLE IF EXISTS `configurations`;
CREATE TABLE `configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `organization_name` varchar(255) NOT NULL,
  `domain_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `meta_title` text NOT NULL,
  `meta_desc` text NOT NULL,
  `timezone` varchar(100) NOT NULL,
  `author` varchar(255) NOT NULL,
  `sms` tinyint(1) NOT NULL,
  `email_notification` tinyint(1) NOT NULL,
  `guest_login` tinyint(1) NOT NULL,
  `front_end` tinyint(1) NOT NULL,
  `slides` tinyint(4) NOT NULL,
  `translate` tinyint(4) NOT NULL DEFAULT '0',
  `paid_exam` tinyint(4) NOT NULL DEFAULT '1',
  `leader_board` tinyint(1) NOT NULL DEFAULT '1',
  `contact` text NOT NULL,
  `photo` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of configurations
-- ----------------------------
BEGIN;
INSERT INTO `configurations` VALUES (1, 'Predicto Tryout Online', 'Mahna', 'predicto.mahna.id', 'admin@mahna.id', 'Tryout Online', 'Tryout Online', 'Asia/Jakarta', 'Exam Solution', 0, 0, 0, 1, 1, 1, 1, 1, '0000-0000~info@mahna.com~http://facebook.com', '', '2014-04-08 20:56:04', '2019-10-01 16:40:41');
COMMIT;

-- ----------------------------
-- Table structure for diffs
-- ----------------------------
DROP TABLE IF EXISTS `diffs`;
CREATE TABLE `diffs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diff_level` varchar(15) NOT NULL,
  `type` char(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of diffs
-- ----------------------------
BEGIN;
INSERT INTO `diffs` VALUES (1, 'Easy', 'E');
INSERT INTO `diffs` VALUES (2, 'Medium', 'M');
INSERT INTO `diffs` VALUES (3, 'Difficult', 'D');
COMMIT;

-- ----------------------------
-- Table structure for exam_groups
-- ----------------------------
DROP TABLE IF EXISTS `exam_groups`;
CREATE TABLE `exam_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_id` (`exam_id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `exam_groups_ibfk_1` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `exam_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for exam_orders
-- ----------------------------
DROP TABLE IF EXISTS `exam_orders`;
CREATE TABLE `exam_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_id` (`exam_id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `exam_orders_ibfk_2` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `exam_orders_ibfk_3` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for exam_questions
-- ----------------------------
DROP TABLE IF EXISTS `exam_questions`;
CREATE TABLE `exam_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_id` (`exam_id`),
  KEY `question_id` (`question_id`),
  CONSTRAINT `exam_questions_ibfk_1` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `exam_questions_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for exam_results
-- ----------------------------
DROP TABLE IF EXISTS `exam_results`;
CREATE TABLE `exam_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `total_question` int(11) NOT NULL,
  `total_answered` int(11) NOT NULL,
  `total_marks` int(11) NOT NULL,
  `obtained_marks` decimal(18,2) NOT NULL,
  `result` varchar(10) NOT NULL,
  `percent` decimal(5,2) NOT NULL,
  `finalized_time` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_id` (`exam_id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `exam_results_ibfk_1` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `exam_results_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for exam_stats
-- ----------------------------
DROP TABLE IF EXISTS `exam_stats`;
CREATE TABLE `exam_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_result_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `ques_no` int(11) NOT NULL,
  `attempt_time` datetime DEFAULT NULL,
  `opened` char(1) NOT NULL DEFAULT '0',
  `answered` char(1) NOT NULL DEFAULT '0',
  `option_selected` varchar(15) NOT NULL,
  `answer` text NOT NULL,
  `true_false` varchar(5) NOT NULL,
  `fill_blank` varchar(100) NOT NULL,
  `marks` int(11) NOT NULL,
  `marks_obtained` decimal(5,2) NOT NULL,
  `closed` char(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `checking_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_id` (`exam_id`),
  KEY `student_id` (`student_id`),
  KEY `question_id` (`question_id`),
  KEY `exam_result_id` (`exam_result_id`),
  CONSTRAINT `exam_stats_ibfk_1` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `exam_stats_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `exam_stats_ibfk_3` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`),
  CONSTRAINT `exam_stats_ibfk_4` FOREIGN KEY (`exam_result_id`) REFERENCES `exam_results` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for exams
-- ----------------------------
DROP TABLE IF EXISTS `exams`;
CREATE TABLE `exams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `instruction` text NOT NULL,
  `duration` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `passing_percent` int(11) NOT NULL,
  `negative_marking` varchar(3) NOT NULL,
  `attempt_count` int(11) NOT NULL,
  `declare_result` varchar(3) NOT NULL DEFAULT 'Yes',
  `finish_result` char(1) NOT NULL DEFAULT '0',
  `ques_random` char(1) NOT NULL DEFAULT '0',
  `paid_exam` char(1) NOT NULL DEFAULT '0',
  `amount` decimal(10,2) DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Inactive',
  `user_id` int(11) NOT NULL,
  `finalized_time` datetime DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_name` (`group_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of groups
-- ----------------------------
BEGIN;
INSERT INTO `groups` VALUES (1, 'Grup Demo', '2019-10-02 10:40:38', '2019-10-02 10:40:38');
COMMIT;

-- ----------------------------
-- Table structure for helpcontents
-- ----------------------------
DROP TABLE IF EXISTS `helpcontents`;
CREATE TABLE `helpcontents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link_title` varchar(255) NOT NULL,
  `link_desc` longtext NOT NULL,
  `status` varchar(8) NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_title` varchar(255) NOT NULL,
  `news_desc` longtext NOT NULL,
  `status` varchar(7) NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for page_rights
-- ----------------------------
DROP TABLE IF EXISTS `page_rights`;
CREATE TABLE `page_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `ugroup_id` int(11) NOT NULL,
  `save_right` int(1) NOT NULL,
  `update_right` int(1) NOT NULL,
  `view_right` int(1) NOT NULL,
  `search_right` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `page_id` (`page_id`),
  CONSTRAINT `page_rights_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of page_rights
-- ----------------------------
BEGIN;
INSERT INTO `page_rights` VALUES (1, 1, 2, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (2, 6, 2, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (3, 5, 2, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (4, 11, 2, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (5, 9, 2, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (6, 7, 2, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (7, 4, 2, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (8, 3, 2, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (9, 2, 2, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (10, 18, 2, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (20, 1, 3, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (21, 6, 3, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (22, 5, 3, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (23, 11, 3, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (24, 18, 3, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (25, 7, 3, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (26, 4, 3, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (27, 3, 3, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (28, 2, 3, 0, 0, 1, 0);
INSERT INTO `page_rights` VALUES (29, 10, 3, 0, 0, 1, 0);
COMMIT;

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_name` varchar(100) NOT NULL,
  `page_name` varchar(100) NOT NULL,
  `controller_name` varchar(100) NOT NULL,
  `action_name` varchar(100) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `parent_id` int(1) NOT NULL,
  `ordering` int(11) NOT NULL,
  `sel_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pages
-- ----------------------------
BEGIN;
INSERT INTO `pages` VALUES (1, 'Dashboard', 'Dashboard', 'admin', '', 'glyphicon-home', 0, 1, NULL);
INSERT INTO `pages` VALUES (2, 'Subjects', 'Subjects', 'Subjects', 'index', 'glyphicon-th-large', 0, 2, 'Questions,Iequestions');
INSERT INTO `pages` VALUES (3, 'Subjects', 'Add Question', 'Addquestions', 'add', '', 2, 3, NULL);
INSERT INTO `pages` VALUES (4, 'Students', 'Students', 'Students', 'index', 'glyphicon-user', 0, 4, NULL);
INSERT INTO `pages` VALUES (5, 'Exams', 'Exams', 'Exams', 'index', 'glyphicon-list-alt', 0, 5, 'Attemptedpapers,Addquestions');
INSERT INTO `pages` VALUES (6, 'Exams', 'Attempted Papers', 'Attemptedpapers', 'index', '', 5, 6, NULL);
INSERT INTO `pages` VALUES (7, 'Results', 'Results', 'Results', 'index', 'glyphicon-asterisk', 0, 7, NULL);
INSERT INTO `pages` VALUES (8, 'Configurations', 'Configurations', 'Configurations', 'index', 'glyphicon-wrench', 0, 8, 'Payments');
INSERT INTO `pages` VALUES (9, 'Help', 'Help', 'Helps', 'index', 'glyphicon-warning-sign', 0, 11, NULL);
INSERT INTO `pages` VALUES (10, 'Users', 'Users', 'Users', 'index', 'glyphicon-user', 0, 9, NULL);
INSERT INTO `pages` VALUES (11, 'Groups', 'Groups', 'Groups', 'index', 'glyphicon-tasks', 0, 3, NULL);
INSERT INTO `pages` VALUES (12, 'Contents', 'Contents', 'Contents', 'index', 'glyphicon-cog', 0, 10, 'News,Slides,Helpcontents');
INSERT INTO `pages` VALUES (13, 'Contents', 'Slides', 'Slides', 'index', '', 12, 99, NULL);
INSERT INTO `pages` VALUES (14, 'Contents', 'Organisation Logo', 'Contents', 'index', '', 12, 99, NULL);
INSERT INTO `pages` VALUES (15, 'Contents', 'News Content', 'News', 'index', '', 12, 99, NULL);
INSERT INTO `pages` VALUES (17, 'Contents', 'Help Content', 'Helpcontent', 'index', '', 12, 99, NULL);
INSERT INTO `pages` VALUES (18, 'Questions', 'Questions', 'Questions', 'add', '', 2, 99, NULL);
INSERT INTO `pages` VALUES (19, 'Payments', 'Paypal Payment', 'Payments', 'index', '', 8, 99, NULL);
COMMIT;

-- ----------------------------
-- Table structure for payments
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `transaction_id` varchar(20) NOT NULL,
  `amount` decimal(18,2) NOT NULL,
  `remarks` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for paypal_configs
-- ----------------------------
DROP TABLE IF EXISTS `paypal_configs`;
CREATE TABLE `paypal_configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `signature` varchar(255) NOT NULL,
  `sandbox_mode` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of paypal_configs
-- ----------------------------
BEGIN;
INSERT INTO `paypal_configs` VALUES (1, '', '', '', 0);
COMMIT;

-- ----------------------------
-- Table structure for qtypes
-- ----------------------------
DROP TABLE IF EXISTS `qtypes`;
CREATE TABLE `qtypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_type` varchar(20) NOT NULL,
  `type` char(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of qtypes
-- ----------------------------
BEGIN;
INSERT INTO `qtypes` VALUES (1, 'Objective Questions', 'M');
INSERT INTO `qtypes` VALUES (2, 'True / False', 'T');
INSERT INTO `qtypes` VALUES (3, 'Fill in the blanks', 'F');
INSERT INTO `qtypes` VALUES (4, 'Subjective', 'S');
COMMIT;

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qtype_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `diff_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `option1` text NOT NULL,
  `option2` text NOT NULL,
  `option3` text NOT NULL,
  `option4` text NOT NULL,
  `option5` text NOT NULL,
  `option6` text NOT NULL,
  `marks` float NOT NULL,
  `negative_marks` float NOT NULL,
  `hint` text NOT NULL,
  `explanation` text NOT NULL,
  `answer` varchar(15) NOT NULL,
  `true_false` varchar(5) NOT NULL,
  `fill_blank` varchar(100) NOT NULL,
  `status` varchar(3) NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`id`),
  KEY `qtype_id` (`qtype_id`),
  KEY `subject_id` (`subject_id`),
  KEY `diff_id` (`diff_id`),
  CONSTRAINT `questions_ibfk_2` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `questions_ibfk_3` FOREIGN KEY (`qtype_id`) REFERENCES `qtypes` (`id`),
  CONSTRAINT `questions_ibfk_4` FOREIGN KEY (`diff_id`) REFERENCES `diffs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for slides
-- ----------------------------
DROP TABLE IF EXISTS `slides`;
CREATE TABLE `slides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slide_name` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `dir` varchar(255) NOT NULL,
  `status` varchar(7) NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for student_groups
-- ----------------------------
DROP TABLE IF EXISTS `student_groups`;
CREATE TABLE `student_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`,`group_id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `student_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `student_groups_ibfk_3` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `guardian_phone` varchar(15) NOT NULL,
  `enroll` varchar(50) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `dir` varchar(255) NOT NULL,
  `status` varchar(7) NOT NULL DEFAULT 'Pending',
  `reg_code` varchar(6) NOT NULL,
  `reg_status` varchar(4) NOT NULL DEFAULT 'Live',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of students
-- ----------------------------
BEGIN;
INSERT INTO `students` VALUES (1, 'demo@student.com', '93b986d2bdb1e8c32d3bcb1debf5586dd2c500d0584b2cc9fb5995d79c848db8', 'Demo Student', 'Jalan Demo', '08971298374', '', '', 'a65b9ded9d5f5f10514480cc8e329835.jpg', '', 'Active', '', 'Done', '2019-10-01 16:37:54', '2019-10-01 16:37:54', '0000-00-00 00:00:00');
COMMIT;

-- ----------------------------
-- Table structure for subjects
-- ----------------------------
DROP TABLE IF EXISTS `subjects`;
CREATE TABLE `subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subject_name` (`subject_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of subjects
-- ----------------------------
BEGIN;
INSERT INTO `subjects` VALUES (1, 'Matematika', '2019-10-02 10:40:12', '2019-10-02 10:40:12');
COMMIT;

-- ----------------------------
-- Table structure for ugroups
-- ----------------------------
DROP TABLE IF EXISTS `ugroups`;
CREATE TABLE `ugroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ugroups
-- ----------------------------
BEGIN;
INSERT INTO `ugroups` VALUES (1, 'Administrator', '2012-07-05 17:16:24', '2012-07-05 17:16:24');
INSERT INTO `ugroups` VALUES (2, 'Instructor', '2012-07-05 17:16:34', '2012-07-05 17:16:34');
INSERT INTO `ugroups` VALUES (3, 'Admin', '2019-10-02 10:25:24', '2019-10-02 10:25:24');
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `mobile` varchar(10) NOT NULL,
  `ugroup_id` int(11) NOT NULL DEFAULT '2',
  `status` enum('Active','Suspend') DEFAULT 'Active',
  `deleted` char(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'superadmin', 'dfb37faf99ffd691383e054541f1a3fd1966273d359d85aa419562fc26bf4427', 'admin@gmail.com', 'Super Administrator', '9839512921', 1, 'Active', NULL, '2014-04-01 21:08:06', '2019-10-02 10:28:21');
INSERT INTO `users` VALUES (9, 'demoteacher', '93b986d2bdb1e8c32d3bcb1debf5586dd2c500d0584b2cc9fb5995d79c848db8', 'demo@teacher.com', 'Teacher Demo', '0182738726', 2, 'Active', NULL, '2019-10-01 16:53:33', '2019-10-01 16:53:33');
INSERT INTO `users` VALUES (10, 'demoadmin', '93b986d2bdb1e8c32d3bcb1debf5586dd2c500d0584b2cc9fb5995d79c848db8', 'demo@admin.com', 'Admin Demo', '0912838921', 3, 'Active', NULL, '2019-10-02 10:27:54', '2019-10-02 10:27:54');
COMMIT;

-- ----------------------------
-- Table structure for wallets
-- ----------------------------
DROP TABLE IF EXISTS `wallets`;
CREATE TABLE `wallets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `in_amount` decimal(18,2) DEFAULT NULL,
  `out_amount` decimal(18,2) DEFAULT NULL,
  `balance` decimal(18,2) NOT NULL,
  `date` datetime NOT NULL,
  `type` varchar(2) NOT NULL,
  `remarks` tinytext NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `student_id_2` (`student_id`),
  CONSTRAINT `wallets_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
